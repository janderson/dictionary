<?php

include_once '../controller/ExceptionController.php';

class Validate {

    public static function decimal($value) {
        str_replace(',', '.', $value);
        if (is_double($value)) {
            return (float) $value;
        } else {
            return 0;
        }
    }

    public static function string($value, $min_lenght) {
        if (count($value) < $min_lenght) {
            return $value;
        } else {
            return NULL;
        }
    }

    public static function integer($value) {
        if (ctype_digit(strval($value))) {
            return strval($value);
        } else {
            return (int) $value;
        }
    }

    public static function cep($cep) {
        return str_replace('-', '', $cep);
    }

    public static function phone($phone) {
        return str_replace(array('-', '.', ' ', '/', '*', ''), '', $phone);
    }

    public static function cpf($cpf) {
        $cpf = str_replace(array('.', '-', ' '), '', $cpf);
        if (self::validateCpf($cpf)) {
            return $cpf;
        } else {
            throw new ExceptionController('Cpf não é válido');
        }
    }

    public static function cnpj($cnpj) {
        $cnpj = str_replace(array('.', '-', ' '), '', $cnpj);
        return $cnpj;
    }

    public static function validateEmail($email) {
        if (!eregi("^[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\-]+\.[a-z]{2,4}$", $email)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    // Validar CEP (xxxxx-xxx)
    public static function validatePostal_Cod($postal_cod) {
        if (!eregi("^[0-9]{5}-[0-9]{3}$", $postal_cod)) {
            return $this->message(1, 'cep', null, null);
        } else {
            return TRUE;
        }
    }

    // Validar Datas (DD/MM/AAAA)
    public static function validateDate($date) {
        if (!eregi("^[0-9]{2}/[0-9]{2}/[0-9]{4}$", $date)) {
            return $this->message(2, 'data', null, null);
        } else {
            return TRUE;
        }
    }

    public static function validateFone($fone) {
//        $string = "(32)55555555";
        if (!eregi("(\([1-9][1-9]\))([0-9]){8,9}", $fone)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    // Validar CPF (111111111111)
    public static function validateCpf($cpf) {

        if (!is_numeric($cpf)) {
            $status = false;
        } else {
            # Pega o digito verificador
            $dv_informado = substr($cpf, 9, 2);

            for ($i = 0; $i <= 8; $i++) {
                $digito[$i] = substr($cpf, $i, 1);
            }
            # Calcula o valor do 10� digito de verifica��o
            $posicao = 10;
            $soma = 0;

            for ($i = 0; $i <= 8; $i++) {
                $soma = $soma + $digito[$i] * $posicao;
                $posicao = $posicao - 1;
            }

            $digito[9] = $soma % 11;

            if ($digito[9] < 2) {
                $digito[9] = 0;
            } else {
                $digito[9] = 11 - $digito[9];
            }

            # Calcula o valor do 11 digito de verificação
            $posicao = 11;
            $soma = 0;

            for ($i = 0; $i <= 9; $i++) {
                $soma = $soma + $digito[$i] * $posicao;
                $posicao = $posicao - 1;
            }

            $digito[10] = $soma % 11;

            if ($digito[10] < 2) {
                $digito[10] = 0;
            } else {
                $digito[10] = 11 - $digito[10];
            }

            # Verifica de o dv � igual ao informado
            $dv = $digito[9] * 10 + $digito[10];

            if ($dv != $dv_informado) {
                $status = false;
            } else {
                $status = true;
            }
        }
        return $status;
    }

    // Validar Numero
    public static function validateNumber($number) {
        if (!is_numeric($number)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    // Validar URL
    public static function validateUrl($url) {
        if (!preg_match('|^http(s)?://[a-z0-9-]+(\.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url)) {
            return $this->message(6, $field, null, null);
        } else {
            return TRUE;
        }
    }

    public static function getWordsNumber($text) {
        return count(split(' ', $text));
    }

// Verificação simples (campos vazios, ultrapassar máximo e mínimo de caracteres)

    public static function validateField($value, $max, $min) {
        if ($value == "") {
            return FALSE;
        } elseif (strlen($value) > $max) {
            return FALSE;
        } elseif (strlen($value) < $min) {
            return FALSE;
        }
        return TRUE;
    }

    public static function clear_string($string) {
//        $chars = array("'", ";", "!", "@", "#", "&", "$", ",", ".", "%", "\"", '\\');
        $chars = array("'", "\\", "\"");
        return str_replace($chars, "", $string);
    }

    public static function formatDateFromPtBr($brDate) {
        $date = str_replace('/', '-', $brDate);
        return date('Y-m-d H:i:s', strtotime($date));
    }

}

?>