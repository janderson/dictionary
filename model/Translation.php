<?php

require_once("Crud.php");

class Translation extends Crud {

    protected $id_language_word1;
    protected $id_language_word2;
    protected $_table = 'TRANSLATION';
    protected $_list = 'lst_word';
    protected $_key_field1 = 'id_language_word1';
    protected $_key_field2 = 'id_language_word2';

    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    function __get($atribute) {
        return $this->$atribute;
    }

    function __construct($id1 = NULL, $id2 = NULL) {
        parent::__construct();
        if ($id1 > 0 || $id2 > 0) {
            $this->find($id1, $id2);
        }
    }

    public function find($id1, $id2) {
        if (!empty($this->_key_field1) && !empty($this->_key_field2)) {
            $return = parent::find(Array($this->_key_field1 => $id1, $this->_key_field2 => $id2));
        } else {
            $return = parent::find(Array($this->_key_field1 => $id1));
        }
        if ($return) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getSerialized($id = NULL) {
        $this->retriveAll(($id != NULL ? Array($this->_key_field => $id) : ''));
        $serialized = serialize($this);
        return $serialized;
    }

    public function __sleep() {
        return array('id_word');
    }

    public function __wakeup() {
        return $this;
    }

}
