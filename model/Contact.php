<?php

require_once("Crud.php");

class Contact extends Crud {

    /**
     * Classe Contact que representa a tabela CONTACT no banmco de dados
     * */
    protected $id_contact; //identificador do contato
    protected $name; //remetente
    protected $email; //remetente
    protected $content; //conteúdo
    protected $subject; //assunto
    protected $_dtt_register;
    protected $id_user;
    protected $_table = 'CONTACT'; //nome da tabela
    protected $_list = 'lst_contact';
    protected $_key_field = 'id_contact';

    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    function __get($atribute) {
        return $this->$atribute;
    }

    function __construct($idcontact = NULL) {
        parent::__construct();
        if ($idcontact > 0) {
            $this->find($idcontact);
        }
    }

    public function find($id) {
        if (parent::find(Array($this->_key_field => $id))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getSerialized($id = NULL) {
        $this->retriveAll(($id != NULL ? Array($this->_key_field => $id) : ''));
        $serialized = serialize($this);
        return $serialized;
    }

    public function __sleep() {
        return array('id_contact', 'subject', 'content', 'name', 'email', 'id_user');
    }

    public function __wakeup() {
        return $this;
    }

    static public function getDescriptionKey($key) {
        try {
            switch ($key) {
                case 'id_contact': return 'ID';
                case 'name': return 'Nome';
                case 'email': return 'E-mail';
                case 'subject': return 'Assunto';
                case 'content': return 'Mensagem';
                case 'id_user': return 'Usuário';
                default : return $key;
            }
        } catch (Exception $exc) {
            return NULL;
        }
    }

    public function getList($rows = 50) {
        try {
            $stmt = $this->_db->prepare("SELECT 
            c.`id_contact` AS `_id`, 
            c.`id_contact` AS `ID`, 
            c.`name`, 
            c.`email`, 
            c.`subject`, 
            u.`name` 
            FROM " . $this->_table . " AS c
            LEFT JOIN USER AS u ON c.`id_user` = u.`id_user`
            ORDER BY c.`dtt_register` DESC LIMIT $rows");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }
}
