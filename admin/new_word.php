<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
include_once '../model/Language.php';
include_once '../model/GramaticalClass.php';
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?php echo Config::TITLE ?></title>
        <link href="../view/css/jquery.steps.css" type="text/css" rel="stylesheet" />
        <link href="../view/css/main.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main>
            <div class="section">
                <?php
                Helper::messageBoxRow();
                ?>
                <div class="row">
                    <section class="col s0 l3">
                        <?php include '../view/partial/adminmenu.php'; ?>
                    </section>
                    <div class="col s12 l9">
                        <div class="row">
                            <div class="card">
                                <div class="card-content">
                                    <?php include '../view/partial/form_new_word.php'; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php include '../view/partial/scripts.php'; ?>
        <script src="../view/js/admin-forms-validation.js"></script>
        <script>
            $("#form-new-word").validate({
                rules: {
                    language: {
                        required: true
                    },
                    word: {
                        required: true
                    },
                    id_gramatical_class: {
                        required: true
                    },
                    phrase_example_first_language: {
                        required: true
                    }
                },
                messages: {
                    language: {
                        required: "Selecione um idioma."
                    },
                    word: {
                        required: "Digite uma palavra."
                    },
                    id_gramatical_class: {
                        required: "Selecione uma Classe Gramatical."
                    },
                    phrase_example_first_language: {
                        required: "Digite uma frase de exemplo com a palavra utilizada."
                    }
                }
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#language').change(function () {
                    id_language = $('.id_language').val();
                    
                    languages = document.getElementById("language-translate");
                    for (i = 0; i < languages.options.length; i++)
                    {
                        if (languages.options[i].value === id_language)
                        {
                            languages.options[i].disabled = true;
                            break;
                        }
                    }
                    console.log(id_language);
                    console.log(languages);
                });
            });
        </script>
        <script src="../view/js/jquery.steps.js"></script>
        <script>
            $("#example-basic").steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                autoFocus: true,
                onStepChanging: function (event, currentIndex, newIndex) {
                    form = $('#form-new-word');
                    form.validate().settings.ignore = ":disabled,:hidden";
                    return form.valid();
                },
                onFinishing: function (event, currentIndex) {
                    form.validate().settings.ignore = ":disabled";
                    return form.valid();
                },
                onFinished: function (event, currentIndex) {
                    $('#form-new-word').submit();
                }
            });
        </script>
    </body>
</html>