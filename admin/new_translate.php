<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
include_once '../model/Word.php';
include_once '../model/Language.php';
include_once '../model/Translation.php';
include_once '../model/GramaticalClass.php';
$word = new Word($_GET['id']);
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?php echo Config::TITLE ?></title>
        <link href="../view/css/jquery.steps.css" type="text/css" rel="stylesheet" />
        <link href="../view/css/main.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main>
            <div class="section">
                <?php
                Helper::messageBoxRow();
                ?>
                <div class="row">
                    <section class="col s0 l3">
                        <?php include '../view/partial/adminmenu.php'; ?>
                    </section>
                    <div class="col s12 l9">
                        <div class="row">
                            <div class="card">
                                <div class="card-content">
                                    <form class="" id="form-new-word" action="../controller/WordController.php" method="POST">
                                        <h3 class="card-title green-text">Palavra</h3>
                                        <section>
                                            <div class="col s12">
                                                <label>Idioma</label>
                                                <?php
                                                $translation = new Translation();
                                                $data_translation = $translation->read(Array("id_word" => $word->id_word));
                                                $language = new Language();
                                                $list = $language->read();
                                                Helper::createSelect(array('list' => $list,
                                                    'id' => 'language',
                                                    'name' => 'language',
                                                    'class' => 'id_language browser-default'), 'id_language', 'name', (!empty($data_translation[0]['id_language']) ? $data_translation[0]['id_language'] : ''));
                                                ?>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s12 l6">
                                                    <input placeholder="Insira a palavra no primeiro idioma" id="word" type="text" name="word" value="<?= $word->word ?>" class="validate">
                                                    <label for="word">Palavra</label>
                                                </div>
                                                <div class="input-field col s12 l6">
                                                    <input placeholder="Insira o plural dessa palavra, caso tenha" id="plural" type="text" value="<?= $word->plural ?>" name="plural" class="validate">
                                                    <label for="plural">Plural da Palavra</label>
                                                </div>
                                            </div>
                                        </section>
                                        <h3 class="card-title green-text">Tradução</h3>
                                        <section>
                                            <div class="col s12">
                                                <label>Idioma</label>
                                                <?php
                                                $language = new Language();
                                                $list = $language->read();
                                                Helper::createSelect(array('list' => $list,
                                                    'id' => 'language-translate',
                                                    'name' => 'language_translate',
                                                    'class' => 'id_language-translate browser-default'), 'id_language', 'name', (!empty($data_translation[0]['id_language']) ? $data_translation[0]['id_language'] : ''));
                                                ?>
                                            </div>
                                            <div class="input-field col s12 l6">
                                                <input placeholder="Insira a tradução da palavra inserida." type="text" class="validate" id="translate" name="translate">
                                                <label for="translate">Tradução</label>
                                            </div>
                                            <div class="input-field col s12 l6">
                                                <input placeholder="Insira a tradução da palavra inserida." type="text" class="validate" id="plural-translate" name="plural_translate">
                                                <label for="plural_translate">Plural da Palavra</label>
                                            </div>
                                        </section>
                                        <h3 class="card-title green-text">Classe Gramatical</h3>
                                        <section>
                                            <div class="col s12 left-align">
                                                <label>Classe Gramatical</label>
                                                <?php
                                                $class = new GramaticalClass();
                                                $list = $class->read();
                                                Helper::createSelect(array('list' => $list,
                                                    'id' => 'id_gramatical_class',
                                                    'name' => 'id_gramatical_class',
                                                    'class' =>
                                                    'browser-default'), 'id_gramatical_class', 'name', NULL);
                                                ?>
                                            </div>
                                            <div class="input-field col s12">
                                                <input placeholder="Insira uma frase utilizando a palavra que vai traduzir" type="text" name="phrase_example_first_language" id="phrase_example" class="validate">
                                                <label for="phrase_example_first_language">Frase de Exemplo no primeiro idioma</label>
                                            </div>
                                            <div class="input-field col s12">
                                                <input placeholder="Insira uma frase utilizando a palavra que traduzida" type="text" name="phrase_example_second_language" id="phrase_example" class="validate">
                                                <label for="phrase_example_second_language">Frase de Exemplo no segundo idioma</label>
                                            </div>
                                        </section>
                                        <input type="hidden" name="id_user" value="<?= ($current_user instanceof Admin ? $current_user->id_admin : $current_user->id_user) ?>" />
                                        <input type="hidden" name="option" value="new_translate" />
                                        <button type="submit" name="option" value="new_translate" class="btn blue lighten-2 white-text button-margin right">Salvar e Enviar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php include '../view/partial/scripts.php'; ?>
        <script src="../view/js/admin-forms-validation.js"></script>
        <script>
            $("#form-new-word").validate({
                rules: {
                    language: {
                        required: true
                    },
                    word: {
                        required: true
                    },
                    id_gramatical_class: {
                        required: true
                    },
                    phrase_example_first_language: {
                        required: true
                    }
                },
                messages: {
                    language: {
                        required: "Selecione um idioma."
                    },
                    word: {
                        required: "Digite uma palavra."
                    },
                    id_gramatical_class: {
                        required: "Selecione uma Classe Gramatical."
                    },
                    phrase_example_first_language: {
                        required: "Digite uma frase de exemplo com a palavra utilizada."
                    }
                }
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#language').change(function () {
                    id_language = $('.id_language').val();

                    languages = document.getElementById("language-translate");
                    for (i = 0; i < languages.options.length; i++)
                    {
                        if (languages.options[i].value === id_language)
                        {
                            languages.options[i].disabled = true;
                            break;
                        }
                    }
                    console.log(id_language);
                    console.log(languages);
                });
            });
        </script>
        <script src="../view/js/jquery.steps.js"></script>
        <script>
            $("#example-basic").steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                autoFocus: true,
                onStepChanging: function (event, currentIndex, newIndex) {
                    form = $('#form-new-word');
                    form.validate().settings.ignore = ":disabled,:hidden";
                    return form.valid();
                },
                onFinishing: function (event, currentIndex) {
                    form.validate().settings.ignore = ":disabled";
                    return form.valid();
                },
                onFinished: function (event, currentIndex) {
                    $('#form-new-word').submit();
                }
            });
        </script>
    </body>
</html>