<?php

require_once '../model/Logger.php';

class ExceptionController extends Exception {

    const URL_ERROR_PAGE = '../view/error.php';
    const URL_ADMIN_LOGIN_PAGE = '../admin/login.php';
    const URL_USER_LOGIN_PAGE = '../view/login.php';
    const INVALID_PASSWORD = '11';
    const INVALID_DATE = '12';
    const INVALID_INT = '13';
    const INVALID_STRING = '13';
    const INVALID_DECIMAL = '13';
    const INVALID_SESSION = '04';
    const INVALID_USER_NAME = '05';
    const INVALID_IDENTIFIER = '06';
    const PAGSEGURO_EXCEPTION = '07';
    const INVALID_OPTION = '08';
    const INVALID_EMAIL = '10';
    const INVALID_CPF = '13';
    const EMPTY_SELECT = '14';
    const EMPTY_INPUT = '15';
    const EMPTY_FIELD = '16';
    const FAIL_REGISTER = '17';
    const FAIL_UPDATE = '18';
    const PDO_EXCEPTION_PREFIX = '91';
    const EXCEPTION_PREFIX = '92';

    public function __construct($message, $code = 0, Exception $previous = null, $exception_type = NULL) {
        // código
        // garante que tudo está corretamente inicializado
        parent::__construct($message, $code, $previous);
        Logger::log_array(__CLASS__, Array("Mensagem:", $message));
    }

    // personaliza a apresentação do objeto como string
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public static function handleException($exception) {
        if ($exception instanceof PDOException) {
            $_SESSION['error_code'] = self::PDO_EXCEPTION_PREFIX . '99';
            $_SESSION['error_message'] = "Houve um problema ao registrar as informações no banco de dados. As informações do problema foram registradas para análise, por favor, entre em contato para relatar o problema. [Código do erro: {$exception->getCode()}]";
            Logger::logFor('exception_unknown', $exception->getCode() . ': ' . $exception->getMessage());
        } elseif ($exception instanceof Exception) {
            $_SESSION['error_code'] = self::EXCEPTION_PREFIX . '99';
            $_SESSION['error_message'] = "{$exception->getMessage()}. [Código do erro: {$exception->getCode()}]";
            Logger::logFor('exception_unknown', $exception->getCode() . ': ' . $exception->getMessage());
        }
        if ($exception->getCode() == '1020') {
            header("Location: " . self::URL_ADMIN_LOGIN_PAGE);
        } else {
            header("Location: " . self::URL_ERROR_PAGE);
        }
    }

}
