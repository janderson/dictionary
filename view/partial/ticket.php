<!-- content -->
<style type="text/css">
    .title {
        font-size: 12pt;
    }
    .ticket {
        margin: 5px;
        border: #cccccc 1px solid;
        box-shadow: gray 2px 1px 0px 0px;
    }
</style>
<table border="0" cellpadding="0" cellspacing="0" width="300" class="ticket">
    <tr>
        <td align="center" valign="top" width="200">
            <table border="0" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td class="leftColumnContent">
                        <img src="http://inventum.org.br/sistema/view/partial/qrcode.php?link={qrcode_link}" style="max-width:200px;" />
                    </td>
                </tr>
                <tr>
                    <td valign="top" >
                        <b>{ticket_code}</b>
                    </td>
                </tr>
                <tr>
                    <td class="rightColumnContent" colspan="2">
                        <b class="title">{activity}</b>
                    </td>
                </tr>
                <tr>
                    <td class="rightColumnContent">
                        <b>{type}</b>
                    </td>
                </tr>
                <tr>
                    <td class="rightColumnContent">
                        Realizador <b>{presenter}</b>
                    </td>
                </tr>
                <tr>
                    <td class="rightColumnContent">
                        Realiz: <b>{location}</b>
                    </td>
                </tr>
                <tr>
                    <td class="rightColumnContent">
                        Início  <b>{begin}</b>
                    </td>
                </tr>
                <tr>
                    <td class="rightColumnContent">
                        Usuário <b>{username}</b>
                    </td>
                </tr>
                <tr>
                    <td class="rightColumnContent">
                        E-mail <b>{useremail}</b>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- /content -->