<?php
$data = array();

function add_person($area, $idioma1, $gram1, $idioma2, $gram2, $idioma3, $gram3, $idioma4) {
    global $data;
    $data [] = array(
        'area' => $area,
        'idioma1' => $idioma1,
        'gram1' => $gram1,
        'idioma2' => $idioma2,
        'gram2' => $gram2,
        'idioma3' => $idioma3,
        'gram3' => $gram3,
        'idioma4' => $idioma4
    );
}

if ($_FILES['file']['tmp_name']) {
    $dom = DOMDocument::load($_FILES['file']['tmp_name']);
    $rows = $dom->getElementsByTagName('Row');
    $first_row = true;
    foreach ($rows as $row) {
        if (!$first_row) {
            $area = "";
            $idioma1 = "";
            $gram1 = "";
            $idioma2 = "";
            $gram2 = "";
            $idioma3 = "";
            $gram3 = "";
            $idioma4 = "";
            $index = 1;
            $cells = $row->getElementsByTagName('Cell');
            foreach ($cells as $cell) {
                $ind = $cell->getAttribute('Index');
                if ($ind != null)
                    $index = $ind;
                if ($index == 1)
                    $area = $cell->nodeValue;
                if ($index == 2)
                    $idioma1 = $cell->nodeValue;
                if ($index == 3)
                    $gram1 = $cell->nodeValue;
                if ($index == 4)
                    $idioma2 = $cell->nodeValue;
                if ($index == 5)
                    $gram2 = $cell->nodeValue;
                if ($index == 6)
                    $idioma3 = $cell->nodeValue;
                if ($index == 7)
                    $gram3 = $cell->nodeValue;
                if ($index == 8)
                    $idioma4 = $cell->nodeValue;
                $index += 1;
            }
            add_person($area, $idioma1, $gram1, $idioma2, $gram2, $idioma3, $gram3, $idioma4);
        }
        $first_row = false;
    }
}
?>
<html>
    <body>
        <table>
            <tr>
                <th>Área</th>
                <th>Gramática</th>
                <th>Português</th>
                <th>Inglês</th>
                <th>Gramática</th>
                <th>Alemão</th>
                <th>Gramática</th>
                <th>Espanhol</th>
            </tr>
            <?php foreach ($data as $row) { ?>
                <tr>
                    <td><?php echo( $row['area'] ); ?></td>
                    <td><?php echo( $row['idioma1'] ); ?></td>
                    <td><?php echo( $row['gram1'] ); ?></td>
                    <td><?php echo( $row['idioma2'] ); ?></td>
                    <td><?php echo( $row['gram2'] ); ?></td>
                    <td><?php echo( $row['idioma3'] ); ?></td>
                    <td><?php echo( $row['gram3'] ); ?></td>
                    <td><?php echo( $row['idioma4'] ); ?></td>
                </tr>
            <?php } ?>
        </table>
    </body>
</html>