<?php
require_once '../model/Crud.php';

class State extends Crud {

    const PLURAL_DESC="Estados";
    const SINGULAR_DESC="Estado";
    
    protected $id_state; //identificador do estado
    protected $uf; //UF
    protected $name; //nome do estado
    protected $_table = "STATE"; //nome da tabela
    protected $_list = 'lst_state'; //nome da view
    protected $_key_field = 'id_state'; //chave do model

    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    function __get($atribute) {
        return $this->$atribute;
    }

    function __construct($idstate = NULL) {
        parent::__construct();
        if ($idstate > 0) {
            $this->find($idstate);
        }
    }
    
    public function find($id) {
        
        if(parent::find(Array($this->_key_field => $id))){
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
?>
