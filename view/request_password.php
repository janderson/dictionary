
<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?= Config::TITLE ?></title>
    </head>
    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main>
            <div class="section">
                <div class="container-fluid">
                    <div class="row">
                        <?php Helper::messageBoxRow(); ?>
                        <div class="col s8 offset-s2 card-panel hoverable">
                            <form class="form-horizontal" method="POST" action="../controller/UserController.php">
                                <h5 class="left-align  grey-text text-darken-2">Requisite uma nova senha</h5>
                                <div class="row">
                                    <div class=" col s12">
                                        <p>Informe o endereço de e-mail utilizado no cadastro e enviaremos uma nova senha para acesso ao sistema. Você poderá modifica-la em sua página de perfil, pelo menu do usuário após realizar seu login.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="email" type="email" name="email" class="validate">
                                        <label for="email">E-mail</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <button name="option" value="request_password" class="btn button-margin waves-effect waves-light darken-1 blue" >Enviar e-mail</button>
                                    <a href="./login.php" class="btn button-margin waves-effect waves-light lighten-2 red" >Cancelar</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php include '../view/partial/scripts.php'; ?>
        <script src="./js/activities.js"></script>
    </body>
</html>
