<html>
    <head>
        <?php
        include './partial/head.php';
        include './partial/scripts.php';
        ?>
        <script>
            jQuery.support.cors = true;
            $.ajax({
                method: "GET",
                url: "https://translate.google.com/#en/pt/Hi",
                success: function (data) {
                    response = JSON.parse(data);
                    console.log(data);
                    $("#modal-code").openModal();
                    $("#close-modal").click(function () {
                        $("#modal-code").closeModal();
                    });
                },
                error: function (request, status, error) {
                    console.log(request.responseText);
                }
            });
        </script>
    </head>
    <body>
        <div class="result"></div>
    </body>
</html>