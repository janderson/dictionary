<div class="col s12 l3 hide-on-med-and-down">
    <div class="row">
        <form class="card white" action="../controller/UserController.php" method="POST" name="login" id="form-login">
            <h4 class="header center green-text">Login</h4>
            <div class="row">
                <div class="input-field col s12">
                    <input type="text" name="login" class="validate">
                    <label for="login">Login</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input type="password" name="password" class="validate">
                    <label for="password">Senha</label>
                </div>
                <div class="col s12">
                    <a href="./request_password.php">Esqueceu sua senha?</a>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button type="submit"  name="option" value="login" class="waves-effect waves-light btn button-margin">LOGIN</button>

                    <a  type="button" class="facebook waves-effect waves-light btn teal darken-3 button-margin" name="entrar" href="./register.php" >
                        Cadastre-se
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <p>É um dos organizadores? <a href="../admin/index.php">Clique Aqui!</a></p>
                </div>
            </div>
        </form>
    </div>
</div>