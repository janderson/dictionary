<!DOCTYPE html>
<?php
include '../partial/session.php';
if (empty($current_user)) {
    getMessage(1102);
}
if (empty($_GET['id'])) {
    header('location: ./lst_report.php');
    exit();
}
include_once '../model/Report.php';
$report = new Report($_GET['id']);
$lst = $report->getReport();
$title = $report->name;
?>
<html>
    <head>
        <title><?= Config::TITLE ?></title>
        <?php
        include '../partial/head.php';
        ?>
        <script type="text/javascript" language="javascript" class="init">

            $(document).ready(function () {
                $('#example').dataTable();
            });
        </script>
    </head>
    <body>
        <section class="container-fluid">
            <?php
            include '../partial/header.php';
            ?>
            <div class="row-fluid">
                <div class="col-lg-12">
                    <?php include '../partial/box_message.php'; ?>
                </div>
                <div class="col-lg-12 col-md-12 ">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo $title ?>
                            <div class="btn-group btn-group-xs pull-right" role="group" aria-label="...">
                                <a href="./lst_report.php" class="btn btn-default"><span
                                        class="glyphicon glyphicon-list-alt"></span> Relatórios</a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table" id="example">
                                <thead>
                                    <tr>
                                        <?php
                                        if (!empty($lst)) {
                                            foreach ($lst[0] as $key => $value) {
                                                if ($key[0] != '_' && $key != 'password') {
                                                    ?>
                                                    <th><?php echo $key; ?></th>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($lst)) {
                                        foreach ($lst as $nrow => $row) {
                                            ?>
                                            <tr class="">
                                                <?php
                                                foreach ($row as $key => $value) {
                                                    if ($key[0] != '_' && $key != 'password') {
                                                        echo "<td>$value</td>";
                                                    }
                                                }
                                                ?>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr class="">
                                            <td>Sem dados para exibição!</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>