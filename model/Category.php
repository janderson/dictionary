<?php

require_once("Crud.php");

class Category extends Crud {

    /**
     * Classe Contact que representa a tabela CONTACT no banmco de dados
     * */
    protected $id_category; 
    protected $name;
    protected $description;
    protected $_table = 'CATEGORY'; //nome da tabela
    protected $_list = 'lst_category';
    protected $_key_field = 'id_category';

    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    function __get($atribute) {
        return $this->$atribute;
    }

    function __construct($idcategory = NULL) {
        parent::__construct();
        if ($idcategory > 0) {
            $this->find($idcategory);
        }
    }

    public function find($id) {
        if (parent::find(Array($this->_key_field => $id))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getSerialized($id = NULL) {
        $this->retriveAll(($id != NULL ? Array($this->_key_field => $id) : ''));
        $serialized = serialize($this);
        return $serialized;
    }

    public function __sleep() {
        return array('id_category', 'name', 'description');
    }

    public function __wakeup() {
        return $this;
    }

    static public function getDescriptionKey($key) {
        try {
            switch ($key) {
                case 'id_category': return 'ID';
                case 'name': return 'Nome';
                case 'description': return 'Descrição';
                default : return $key;
            }
        } catch (Exception $exc) {
            return NULL;
        }
    }

    public function getList($rows = 50) {
        try {
            $stmt = $this->_db->prepare("SELECT 
            c.`id_category` AS `_id`, 
            c.`id_category` AS `ID`, 
            c.`name`, 
            c.`description`
            FROM " . $this->_table . " 
            ORDER BY c.`dtt_register` DESC LIMIT $rows");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }
}
