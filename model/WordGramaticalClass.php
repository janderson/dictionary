<?php

require_once("Crud.php");

class WordGramaticalClass extends Crud {

    protected $id_word;
    protected $id_gramatical_class; 
    protected $example_sentence;
    protected $_table = 'WORD_GRAMATICAL_CLASS';
    protected $_list = 'lst_word';
    protected $_key_field1 = 'id_word';
    protected $_key_field2 = 'id_gramatical_class';

    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    function __get($atribute) {
        return $this->$atribute;
    }

    function __construct($idword = NULL, $idgramatical_class = NULL) {
        parent::__construct();
        if ($idword > 0 || $idgramatical_class > 0) {
            $this->find($idword, $idgramatical_class);
        }
    }

    public function find($idword, $idgramatical_class) {
        if (!empty($this->_key_field1) && !empty($this->_key_field2)) {
            $return = parent::find(Array($this->_key_field1 => $idword, $this->_key_field2 => $idgramatical_class));
        } else {
            $return = parent::find(Array($this->_key_field1 => $idword));
        }
        if ($return) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getSerialized($id = NULL) {
        $this->retriveAll(($id != NULL ? Array($this->_key_field => $id) : ''));
        $serialized = serialize($this);
        return $serialized;
    }

    public function __sleep() {
        return array('id_word');
    }

    public function __wakeup() {
        return $this;
    }
}
