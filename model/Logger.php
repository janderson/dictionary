<?php

if (!class_exists('Logger')) {

    class Logger {

        const FILE_PATH = "/log/";
        const FILE_NAME = "log";

        public function __construct() {

            date_default_timezone_set('America/Sao_Paulo');
        }

        private static function getLogPath() {
            return realpath('../') . self::FILE_PATH;
        }

        public static function log_string($msg) {
            Logger::gravar(Logger::FILE_NAME, $msg, FALSE);
        }

        public static function log_array($filename, $array, $local = NULL) {

            $msg = implode("; ", $array);
            Logger::gravar($filename, $msg, $local);
        }

        public static function logFor($filename, $msg) {
            Logger::gravar($filename, $msg);
        }

        public static function gravar($filename, $msg, $local = NULL, $data_on_name = TRUE) {
            try {
                $data = date("d-m-y");
                if ($data_on_name) {
                    $filename .= '-' . $data;
                }
                $hora = date("H:i:s");
                $ip = $_SERVER['REMOTE_ADDR']; //Nome do arquivo:
                if ($local != NULL) {
                    $method = $local;
                } else {
                    $method = '';
                }
                $arquivo = self::getLogPath() . "$filename.log";

                //Texto a ser impresso no log:
                $texto = "[$data][$hora][$ip][$method]> $msg \n";

                $manipular = fopen("$arquivo", "a+b");
                fwrite($manipular, $texto);
                fclose($manipular);
            } catch (Exception $ex) {
                var_dump($ex);
            }
        }

    }

}
?>