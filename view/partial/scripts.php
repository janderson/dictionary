<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="../view/js/materialize.js"></script>
<script type="text/javascript" src="../view/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="../view/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../view/js/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="../view/js/form-validation-script.js"></script>
<script src="../view/js/init.js"></script>
