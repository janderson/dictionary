<?php

require_once '../model/Logger.php';

class CrudException extends Exception {

    public function __construct($message, $code = 0, Exception $previous = null, $exception_type = NULL) {
        // código
        // garante que tudo está corretamente inicializado
        parent::__construct($message, $code, $previous);
    }

    // personaliza a apresentação do objeto como string
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

}

class Crud {
    /* Classe responsável por gerenciar as operações básicas que envolvem Banco de Dados
     * *tendo uma ligação direta com todos os models que referenciam as tabelas do projeto.
     * *
     * */

    protected $_db; //atributo do nome do BD

    public function __construct() {
        /* Método construtor, responsável por realizar a conexão com o BD */
        try {
            $host = 'localhost'; //Servidor
            $data_base = 'dicionariomulti'; //Nome do Banco de Dados
            $user = 'dictionary_utfpr'; //Usuário
            $password = 'utf_pb_dic'; //Senha de acesso
            $colation = 'utf8'; //Colação
            $this->_db = new PDO("mysql:host=$host;dbname=$data_base", "$user", "$password"); //String de conexão
            $this->_db->exec("set names $colation"); //Setando a colação
        } catch (Exception $e) {
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    public function __desctruct() {
        /* Método respónsável por desfazer a conexão com o BD ao desconstruir o objeto */
        $this->_db = null; //Atribui NULL para o BD
    }

    public function disconnectDB() {
        /*  */
        $this->_db = null; //Atribui NULL para o BD
    }

    public function create() {
        /* Método responsável por preparar e inserir dados no BD
         * 
         * * @param NO_PARAM
         * * @return INT $id ou NULL
         * */

        /* Obter os valores e as chaves para montar o sql do insert */
        try {
            foreach (get_class_vars(get_class($this)) as $key => $value) {
                if ($key[0] != '_') {
                    $fields[] = $key;
                    $values[] = $this->clear($this->$key);
                }
            }
            //Implodir o array dos campos em string
            $fields = implode(", ", $fields);
            //obter os valores respectivos aos campos
            $values = $this->getCreateValues($values);
            $insert = "INSERT INTO {$this->_table} ({$fields}) VALUES ({$values})"; //atribuindo o sql a uma variável
            $stmt = $this->_db->prepare("$insert"); //preparando o comando PDO
            $id = NULL; //atribuindo NULL aop id
            $this->_db->setAttribute(PDO::ATTR_AUTOCOMMIT, 0); //iniciando o setAtribute como 0
            $this->_db->beginTransaction(); //inicio da Transação
            if ($stmt->execute() && $stmt->rowCount() > 0) {//executando o response
                $id = $this->_db->lastInsertId();
                $this->_db->commit(); //realizando commit
                return $id;
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
                throw new CrudException($stmt->errorInfo()[2], $stmt->errorInfo()[1]);
            }
        } catch (PDOExecption $e) { //verificando erros
            Logger::log_array(__CLASS__, $e);
            $this->_db->rollback(); //realizando rollback
            throw $e;
        } finally {
            $this->_db->setAttribute(PDO::ATTR_AUTOCOMMIT, 1); //finalizando o setAtribute como 1
        }
    }

    public function createManyToMany() {
        /* Método responsável por preparar e inserir dados no BD
         * 
         * * @param NO_PARAM
         * * @return INT $id ou NULL
         * */

        /* Obter os valores e as chaves para montar o sql do insert */
        try {
            foreach (get_class_vars(get_class($this)) as $key => $value) {
                if ($key[0] != '_') {
                    $fields[] = $key;
                    $values[] = $this->clear($this->$key);
                }
            }
            //Implodir o array dos campos em string
            $fields = implode(", ", $fields);
            //obter os valores respectivos aos campos
            $values = $this->getCreateValues($values);
            $insert = "INSERT INTO {$this->_table} ({$fields}) VALUES ({$values})"; //atribuindo o sql a uma variável
            $stmt = $this->_db->prepare("$insert"); //preparando o comando PDO
            $id = NULL; //atribuindo NULL aop id
            if ($stmt->execute()) {//executando o response
                $id = $this->_db->lastInsertId(); //obtendo o último id gerado
                $this->_db->commit(); //realizando commit
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
            }
        } catch (PDOExecption $e) { //verificando erros
            Logger::log_array(__CLASS__, $e);
            throw $e;
        } finally {
            return $id;
        }
    }

    public function read($filter = null) {
        /* Médoto responsável por retornar busca na tabela atribuida
         * 
         * @param ARRAY $filter array com os filtros
         * @return ARRAY $response dados obtidos no banco
         */
        try {
            $where = $this->mountWhere($filter); //Montando WHERE para filtrar

            $select = "SELECT * FROM {$this->_table} {$where} ORDER BY 1"; //atribuindo a string do sql
            $stmt = $this->_db->prepare("$select"); //preparando o comando PDO
            if ($stmt->execute()) { //executando a resposta
                $stmt->setFetchMode(PDO::FETCH_ASSOC); //realizando o fetchmode na resposta
                return $stmt->fetchAll(); //transformando em array a resposta e retornando-a
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
            }
        } catch (PDOExecption $e) { //verificando erros
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    public function readView($filter = null) {
        /* Médoto responsável por retornar busca na view atribuida
         * 
         * @param ARRAY $filter array com os filtros
         * @return ARRAY $response dados obtidos no banco a partir view
         */
        try {
            $where = $this->mountWhere($filter); //Montando WHERE para filtrar

            $select = "SELECT * FROM $this->_list $where"; //atribuindo a string do sql
            $stmt = $this->_db->prepare("$select"); //preparando o comando PDO
            if ($stmt->execute()) { //executando a resposta)
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll(); //transformando em array a resposta e retornando-a
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
            }
        } catch (PDOExecption $e) { //verificando erros
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    public function readNView($view, $filter = null) {
        /* Médoto responsável por retornar busca na view atribuida
         * 
         * @param ARRAY $filter array com os filtros
         * @return ARRAY $response dados obtidos no banco a partir view
         */
        try {
            $where = $this->mountWhere($filter); //Montando WHERE para filtrar

            $select = "SELECT * FROM $view $where"; //atribuindo a string do sql
            $stmt = $this->_db->prepare("$select"); //preparando o comando PDO
            if ($stmt->execute()) { //executando a resposta)
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll(); //transformando em array a resposta e retornando-a
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
            }
        } catch (PDOExecption $e) { //verificando erros
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    public function find($filter) {
        /* Médoto responsável por retornar busca na view atribuida
         * 
         * @param ARRAY $filter filtro para realizar buscar
         * @return ARRAY $response dados obtidos no banco pela view
         */
        try {
            $row_data = $this->read($filter);

            /* Obter os valores e as chaves para dar um estado ao objeto */
            if (count($row_data) > 0) {
                $class_attr = get_class_vars(get_class($this));
                foreach ($row_data[0] as $key => $value) {
                    $this->$key = $value;
                }
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (Exception $e) {
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    public function retrive($filter) {

        /**
         * (PHP 5 &gt;= 5.1.0, PECL pdo &gt;= 0.1.0)<br/>
         * Busca o registro no banco de dados e retorna um objeto com estado
         * @param array $filter filtro para realizar a busca
         * @return object  <b>__CLASS__</b>
         */
        try {
            $where = $this->mountWhere($filter); //Montando WHERE para filtrar

            $select = "SELECT * FROM {$this->_table} {$where} ORDER BY 1"; //atribuindo a string do sql
            $stmt = $this->_db->prepare("$select"); //preparando o comando PDO
            if ($stmt->execute()) { //executando a resposta
                $stmt->setFetchMode(PDO::FETCH_CLASS, get_class($this));
                //transformando em array a resposta e retornando-a
                return $stmt->fetchObject(get_class($this));
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
                return NULL;
            }
        } catch (PDOExecption $e) { //verificando erros
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    public function retriveAll($filter) {
        /* Médoto responsável por retornar busca na view atribuida
         * 
         * @param ARRAY $filter filtro para realizar buscar
         * @return ARRAY $response dados obtidos no banco pela view
         */
        try {
            $where = $this->mountWhere($filter); //Montando WHERE para filtrar

            $select = "SELECT * FROM {$this->_table} {$where} ORDER BY 1"; //atribuindo a string do sql
            $stmt = $this->_db->prepare("$select"); //preparando o comando PDO
            if ($stmt->execute()) { //executando a resposta
                $stmt->setFetchMode(PDO::FETCH_CLASS, get_class($this)); //realizando o fetchmode na resposta
                return $stmt->fetchAll(); //transformando em array a resposta e retornando-a
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
                return NULL;
            }
        } catch (PDOExecption $e) { //verificando erros
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

    public function update(Array $filter, Array $fields = NULL) {
        /* Médoto responsável por alterar registros específicos no BD
         * 
         * @param ARRAY $filter array com filtros
         * @return BOOL TRUE ou FALSE
         */
        try {
            $where = $this->mountWhere($filter); //Montando WHERE para filtrar
            /* Obter os valores e as chaves para montar o sql do update */
            if ($fields != NULL) {
                foreach ($fields as $key => $value) {
                    /* Verifica se o primeiro caractere do campo não é _ e se o campo existe na classe */
                    if ($key[0] != '_' && array_key_exists($key, get_class_vars(get_class($this)))) {
                        /* monta o array de valores enviando para outro método o valor para ser validado */
                        $values[] = "{$key} = {$this->getValueForSql($value)}";
                    }
                }
            } else {
                foreach (get_class_vars(get_class($this)) as $key => $value) {
                    if ($key[0] != '_') {
                        $values[] = "{$key} = {$this->getValueForSql($this->$key)}";
                    }
                }
            }
            //Implodir o array dos campos em string
            $values = implode(", ", $values);
            //retirando a virgula do último registro
            substr_replace($values, '', 0, 2);
            $update = "UPDATE `{$this->_table}` SET {$values} {$where}"; //atribuindo string de Update
            $stmt = $this->_db->prepare("$update"); //preparando comando PDO

            $this->_db->setAttribute(PDO::ATTR_AUTOCOMMIT, 0); //iniciando o setAtribute como 0
            $this->_db->beginTransaction(); //inicio da Transação
            if ($stmt->execute()) {
                $this->_db->commit();
                return $stmt->rowCount();
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
                throw new Exception($stmt->errorInfo()[2], $stmt->errorInfo()[1]);
            }
        } catch (PDOExecption $e) { //verificando erros
            $this->_db->rollback(); //realizando rollback
            throw $e;
        } finally {
            $this->_db->setAttribute(PDO::ATTR_AUTOCOMMIT, 1); //finalizando o setAtribute como 1
        }
    }

    public function updateManyToMany(Array $filter, Array $fields = NULL) {
        /* Médoto responsável por alterar registros específicos no BD
         * 
         * @param ARRAY $filter array com filtros
         * @return BOOL TRUE ou FALSE
         */
        try {
            $where = $this->mountWhere($filter); //Montando WHERE para filtrar
            /* Obter os valores e as chaves para montar o sql do update */
            if ($fields != NULL) {
                foreach ($fields as $key => $value) {
                    /* Verifica se o primeiro caractere do campo não é _ e se o campo existe na classe */
                    if ($key[0] != '_' && array_key_exists($key, get_class_vars(get_class($this)))) {
                        /* monta o array de valores enviando para outro método o valor para ser validado */
                        $values[] = "{$key} = {$this->getValueForSql($value)}";
                    }
                }
            } else {
                foreach (get_class_vars(get_class($this)) as $key => $value) {
                    if ($key[0] != '_') {
                        $values[] = "{$key} = {$this->getValueForSql($this->$key)}";
                    }
                }
            }
            //Implodir o array dos campos em string
            $values = implode(", ", $values);
            //retirando a virgula do último registro
            substr_replace($values, '', 0, 2);
            $update = "UPDATE {$this->_table} SET {$values} {$where}"; //atribuindo string de Update
            $stmt = $this->_db->prepare("$update"); //preparando comando PDO
            if ($stmt->execute()) {
                return $stmt->rowCount();
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
            }
        } catch (PDOExecption $e) { //verificando erros
            throw $e;
        } finally {
            
        }
    }

    public function delete(Array $filter) {
        /* Método básico para deletar registros no BD
         * 
         * @param ARRAY $filter array com filtros
         * @return BOOL TRUE ou FALSE
         */
        try {
            $where = $this->mountWhere($filter); //montando string do where
            //obtendo array de campos e valores
            foreach (get_class_vars(get_class($this)) as $key => $value) {
                if ($key[0] != '_') {
                    $fields[] = "{$key} = {$this->getValueForSql($this->$key)}";
                }
            }

            $fields = implode(", ", $fields); //implodindo array em string
            substr_replace($fields, '', 0, 2); //retirando virgula do último campo
            $fields = "DELETE FROM {$this->_table} {$where}"; //atribuindo string do sql para deletar
            $stmt = $this->_db->prepare("$fields"); //preparando comando PDO
            $this->_db->setAttribute(PDO::ATTR_AUTOCOMMIT, 0); //iniciando o setAtribute como 0
            $this->_db->beginTransaction(); //inicio da Transação
            if ($stmt->execute()) {
                return $stmt->rowCount();
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
            }
        } catch (PDOExecption $e) { //verificando erros
            $this->_db->rollback(); //realizando rollback
        } finally {
            $this->_db->setAttribute(PDO::ATTR_AUTOCOMMIT, 1); //finalizando o setAtribute como 1
        }
    }

    private function mountWhere($filter) {
        /* método responsável por montar a estrutura de filtros quando necessário
         * @param FILTER filtro de string
         * @return STRING $where string com filtro sql
         * */
        try {
            $where = "WHERE (1 = 1) "; //inicia a estrutura where para não haver conflitos
            if ($filter != null) { //verifica se existe filtro
                //monta a string que filtrará a busca
                foreach ($filter as $key => $value) {
                    if (strtolower(substr($key, 0, 2)) == 'id') {
                        $where = $where . " AND (`{$key}` = '$value')";
                    } else {
                        $where = $where . " AND (LOWER(`{$key}`) LIKE LOWER('{$value}'))";
                    }
                }
            }
            return $where; //retornando string do sql
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function getCreateValues($values) {
        /**
         * Obter valores do create
         * 
         * @param ARRAY $values array com valores
         * @return STRING string
         */
        try {
            $sql = ''; //inicia a variável sql
            //montar string do sql
            foreach ($values as $key => $value) {
                if (is_int($value) || is_double($value)) {
                    $sql .= $value . ', ';
                } elseif ($value == NULL || $value == '') {
                    $sql .= 'NULL, ';
                } else {
                    $sql .= "'$value', ";
                }
            }
            return substr_replace($sql, '', -2, 2); //retornando uma substring do sql
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function getValueForSql($value) {
        /* Médoto responsável por validar o valor recebido. identificando se é string, inteiro ou double
         * @param INDEFINIDO $value valor do array enviado
         * @return INDERFINIDO $value
         */
        try {
            if ($value == NULL || $value == '') { //verifica se é nulo ou string vazia
                return 'NULL'; //retorna null em string
            } else if (is_int($value) || is_double($value)) { //verifica se é interiro ou double
                return $value; //retorna o valor sem alterações
            } else {
                return "'{$this->clear($value)}'"; //retorna o valor como string
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function clear($string) {
        //$chars = array("'", ";", "!", "@", "#", "&", "$", ",", ".", "%", "\"", '\\');
        $chars = array("'", "\\", "--", "..", "%", "/", "../", "//"); //especifica os caracteres que serão retirados da string
        return str_replace($chars, "", $string); //realiza a busca e limpeza da string
    }

    private function vl_string($string) {
        //$chars = array("'", ";", "!", "@", "#", "&", "$", ",", ".", "%", "\"", '\\');
        $chars = array("'", ";", '.', '-', '(', ')'); //especifica os caracteres que serão retirados da string
        return str_replace($chars, "", $string); //realiza a busca e limpeza da string
    }

    public function searchValue($field, $value, $id = NULL) {
        /*
         * Método para validação de dados do usuário
         * 
         * @param String $field Field campo para pesquisa
         * @param String $id Identificador para comparação
         * @param String $id Identificador para comparação
         * @return BOOL TRUE se uma ou mais ocorrencias encontradas
         */
        try {
            $where = $this->mountWhere(array($field => $value)); //Montando WHERE para filtrar

            $select = "SELECT * FROM {$this->_table} {$where} "
                    . (!empty($id) ? " AND $this->_key_field != $id" : "")
                    . " ORDER BY 1"; //atribuindo a string do sql
            $stmt = $this->_db->prepare("$select"); //preparando o comando PDO
            if ($stmt->execute()) { //executando a resposta
                return $stmt->rowCount() > 0; //realizando o fetchmode na resposta
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
                return FALSE;
            }
        } catch (PDOExecption $e) { //verificando erros
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

}

/* Obter os valores e as chaves para dar um estado ao objeto*/
//        $class_attr = get_class_vars(get_class($this));
//        foreach ($row_data[0] as $key => $value) {
//            $this->$key = $value;
//        }