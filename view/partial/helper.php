<?php
require_once '../model/Admin.php';
require_once '../model/User.php';
require_once '../model/Config.php';

class Helper {

    const URL_LOGIN = './login.php';
    const URL_LOGIN_USER = '../view/login.php';
    const URL_LOGIN_ADMIN = '../admin/login.php';
    const URL_USER_CONTROL_PANEL = '../view/control_panel.php';
    const USER_SESSION = 0;
    const ADMIN_SESSION = 1;

    public static function bootSession() {
        ob_start();
        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
        }
        if (self::isLoged()) {
            $email = $_SESSION['email'];
            $password = $_SESSION['password'];
            if ($_SESSION['type_user'] == 0 && (new User())->validateUser($email, $password)) {
                return (new User())->retriveUser($email);
            } else if ($_SESSION['type_user'] == 1 && (new Admin())->validateAdmin($email, $password)) {
                return (new Admin())->retriveAdmin($email);
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    public static function setCurrentUser($user) {
        /**
         * Adiciona uma sessão de usuário para administrador ou participante
         */
        if ($user instanceof User) {
            $_SESSION['id_user'] = $user->id_user;
            $_SESSION['name'] = $user->name;
            $_SESSION['email'] = $user->email;
            $_SESSION['password'] = $user->password;
            $_SESSION['type_user'] = self::USER_SESSION;
        } elseif ($user instanceof Admin) {
            $_SESSION['id_admin'] = $user->id_admin;
            $_SESSION['name'] = $user->name;
            $_SESSION['email'] = $user->email;
            $_SESSION['password'] = $user->password;
            $_SESSION['type_user'] = self::ADMIN_SESSION;
        } else {
            return NULL;
        }
    }

    public static function isLoged() {
        /**
         * Retorna se há um usuário logado
         */
        return (isset($_SESSION['email'])) && isset($_SESSION['password']);
    }

    /* FUNÇÕES PARA SEREM UTILIZADAS MAJORITARIAMENTE NAS VIEWS */

    public static function requireSession($current_user) {
        /**
         * Necessário haver a sessão de algum Administrador ou User
         */
        if (empty($current_user)) {
            header('location: ' . Helper::handleError('1010'));
            exit();
        }
    }

    public static function requireAdminSession($current_user, $required_credential = array()) {
        /**
         * Necessário haver a sessão de algum Administrador
         */
        if (!($current_user instanceof Admin && $current_user->validateCredentials($required_credential))) {
            header('location: ' . Helper::handleError('2010'));
            exit();
        }
    }

    public static function requireUserSession($current_user) {
        /**
         * Necessário haver a sessão de algum Usuário
         */
        if (!$current_user instanceof User) {
            header('location: ' . Helper::handleError('1010'));
            exit();
        }
    }

    public static function requireNoSession($current_user) {
        /**
         * Nenhum usuário deve estar logado
         */
        if (!empty($current_user)) {
            header('location: ' . self::URL_USER_CONTROL_PANEL);
            exit();
        }
    }

    public static function handleError($error_code) {
        switch ($error_code) {
            case '1010':
                $_SESSION['error_code'] = $error_code;
                $_SESSION['error_message'] = 'Você não tem permissão para acessar esta página, por favor realize o login.';
                return '../view/login.php';
            case '2010':
                $_SESSION['error_code'] = $error_code;
                $_SESSION['error_message'] = 'Você não tem permissão para acessar esta página, por favor realize o login de administrador.';
                return '../admin/login.php';
            case '9903':
                $_SESSION['error_code'] = $error_code;
                $_SESSION['error_message'] = 'Não foi possível obter o login via facebook.';
                return '../admin/login.php';
            case '1037':
                $_SESSION['error_code'] = $error_code;
                $_SESSION['error_message'] = 'Você não tem permissão para acessar esta página, ';
                return '../view/login.php';
            case '5010':
                $_SESSION['error_code'] = NULL;
                $_SESSION['error_message'] = NULL;
                return '../admin/lst_admin.php';
            case '5020':
                $_SESSION['error_code'] = NULL;
                $_SESSION['error_message'] = NULL;
                return '../admin/lst_presenter.php';
            case '5030':
                $_SESSION['error_code'] = NULL;
                $_SESSION['error_message'] = NULL;
                return '../admin/lst_activity.php';
            case '5040':
                $_SESSION['error_code'] = NULL;
                $_SESSION['error_message'] = NULL;
                return '../admin/lst_institution.php';
            case '5050':
                $_SESSION['error_code'] = NULL;
                $_SESSION['error_message'] = NULL;
                return '../admin/lst_user.php';
            case '7011':
                $_SESSION['error_code'] = $error_code;
                $_SESSION['error_message'] = 'O código do QRCode não foi identificado';
                return '../admin/lst_institution.php';
            case '7012':
                $_SESSION['error_code'] = $error_code;
                $_SESSION['error_message'] = 'O Ticket não pode ser verificado com este código';
                return '../admin/lst_institution.php';
            default:
                break;
        }
    }

    public static function messageBoxRow($clean_message = TRUE) {
//        if (empty($_SESSION['error_code']) && empty($_SESSION['sucess'])) {
//            $_SESSION['sucess'] = 'Testando linha de mensagens';
//        } // TESTES

        if (isset($_SESSION['error_code'])) {
            echo '<div class="red lighten-3 message-box">';
            echo $_SESSION['error_message'];
            $code = $_SESSION['error_code'];
            echo '</div>';
        } elseif (isset($_SESSION['sucess'])) {
            echo '<div class="teal lighten-4 message-box">';
            echo $_SESSION['sucess'];
            echo '</div>';
        }
        if ($clean_message) {
            unset($_SESSION['sucess']);
            unset($_SESSION['error_code']);
            unset($_SESSION['error_message']);
        }
    }

    public static function createTable($model, $url_edt = NULL, $url_dtl = NULL, $htmlClass = 'bordered', $line_number = FALSE) {
        /* Método para gerar as tags de Select para formulários.

         * @param String $name nome do campo;         
         * @param String $id identificador do campo;         
         * @param String $list array de dados para gerar os options do campo;         
         * @param String $action_js método javascript do campo (opcional);         
         */
        require_once "../model/$model.php";
        $object = new $model();
        $lst = $object->getList();
        $colNum = 0;
        echo "<table class='{$htmlClass}' cellspacing='0' width='100%'><thead><tr>";
        if (!empty($lst)) {
            if ($line_number) {
                echo "<th>n º</th>";
                $colNum += 1;
            }
            foreach ($lst[0] as $key => $value) {
                if ($key[0] != '_') {
                    $colNum += 1;
                    echo "<th>{$object::getDescriptionKey($key)}</th>";
                }
            }
            if (!empty($url_dtl) || !empty($url_edt)) {
                $colNum += 1;
                echo "<th>Opções</th>";
            }
        }
        echo "</tr></thead><tbody>";
        if (!empty($lst)) {
            foreach ($lst as $nrow => $row) {
                echo "<tr> ";
                if ($line_number) {
                    echo "<td>" . ($nrow + 1) . "</td>";
                }
                foreach ($row as $key => $value) {
                    if ($key[0] != '_') {
                        echo "<td>$value</td>";
                    }
                }
                if (!empty($url_dtl) || !empty($url_edt)) {
                    echo "<td><div class='' role='group' >";
                    if (!empty($url_dtl)) {
                        self::createLink($url_dtl, $row['_id'], "zoom_in", "Detalhes");
                    }
                    if (!empty($url_edt)) {
                        self::createLink($url_edt, $row['_id'], "assignment", "Editar");
                    }
                    echo "</div></td>";
                }
                echo '</tr>';
            }
        } else {
            echo "<tr class=''><td>Sem dados para exibição!</td></tr>";
        }
        echo "</tbody><tfoot><tr>";
        if (!empty($lst)) {
            $colNum -= 1;
            echo "<th colspan='{$colNum}'>Total</th>";
            echo "<th>" . count($lst) . "</th>";
        }

        echo "</tr></tfoot></table>";
    }

    public static function getArrayAlphabet() {
        $array = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'J', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        return $array;
    }

    public static function createTabs($array) {
        ?>
        <div class="row">
            <div class="col s11 padding-none-alphabet">
                <div class="col s6 padding-none-alphabet">
                    <?php
                    foreach ($array AS $row => $tab) {
                        if ($row < 24) {
                            if ($row == 12) {
                                ?>
                            </div>
                            <div class="col s6 padding-none-alphabet">
                                <?php
                            }
                            ?>
                            <a href="../controller/WordController.php?option=filter_word&letter=<?= $tab ?>">
                                <div class="col s1"><?= $tab ?></div>
                            </a>
                            <?php
                            $row++;
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="col s1 padding-none-alphabet">
                <?php
                foreach ($array AS $row => $tab) {
                    if ($row >= 24) {
//                            if ($cont == 12) {
                        ?>
                        <!--                            </div>
                                                    <div class="col s6 padding-none-alphabet">-->
                        <?php
//                            }
                        ?>
                        <a href="../controller/WordController.php?option=filter_word&letter=<?= $tab ?>">
                            <div class="col s4"><?= $tab ?></div>
                        </a>
                        <?php
                        $row++;
                    }
                }
                ?>
            </div>
        </div>
        <?php
    }

    public static function createTableNovo(array $array) {
        $colNum = 0;
        echo "<table class='" . (is_string($array['html_class']) ? $array['html_class'] : '') . "' cellspacing='0' width='100%'>";
        if ($array['list'] === '' || count($array['list']) > 0) {
            if (empty($array['list']) && !is_array($array['list'])) {
                require_once "../model/{$array['model']}.php";
                $object = new $array['model']();
                $array['list'] = $object->getList();
            }
            $colNum = self::createThead($array, $colNum);
            echo "<tbody>";
            if (!empty($array['list']) && is_array($array['list'])) {
                $colNum = self::createTbody($array, $colNum);
            } else {
                require_once "../model/{$array['model']}.php";
                $object = new $array['model']();
                $array['list'] = $object->getList();

                if (!empty($array['list']) && is_array($array['list'])) {
                    $colNum = self::createTbody($array, $colNum);
                } else {
                    echo "<tr class=''><td>Sem dados para exibição!</td></tr>";
                }
            }
        } else {
            echo "<tr class=''><td>Sem dados para exibição!</td></tr>";
        }
        echo "</tbody>";
        echo "<tfoot>";
        echo "<tr>";
        if (!empty($array['list'])) {
            $colNum -= 1;
            echo "<th colspan='{$colNum}'>Total</th>";
            echo "<th>" . count($array['list']) .
            "</th>";
        }
        echo "</tr>";
        echo "</tfoot>";
        echo "</table>";
    }

    public static function createThead(array $array, $colNum = NULL) {
        echo "<thead>";
        echo "<tr>";
        if (!empty($array['list']) && is_array($array['list'])) {
            if (is_bool($array['line_number']) && $array['line_number']) {
                echo "<th>n º</th>";
                $colNum += 1;
            }
            foreach ($array ['list'][0] as $key => $value) {
                if ($key[0] != '_') {
                    $colNum += 1;
                    if (!empty($array['model'])) {
                        echo "<th>" . (is_string($array['model']) ? $array['model']::getDescriptionKey($key) : $key) . "</th>";
                    } else {
                        echo "<th>" . $key . "</th>";
                    }
                }
            }
            if (!empty($array['options'])) {
                $colNum += 1;
                echo "<th class='right-align'>Opções</th>";
            }
        }
        echo "</tr>";
        echo "</thead>";
        if (!empty($colNum)) {
            return $colNum;
        }
    }

    public static function createTbody(array $array, $colNum = NULL) {
        foreach ($array['list'] as $nrow => $row) {
            echo "<tr> ";
            if (is_bool($array['line_number']) && $array['line_number']) {
                echo "<td>" . ($nrow + 1) . "</td>";
            }
            foreach ($row as $key => $value) {
                if ($key[0] != '_') {
                    echo "<td>$value</td>";
                }
            }
            if (!empty($array['options'])) {
                echo "<td class='right-align'>";
                foreach ($array['options'] AS $option) {
                    self::createLink($option['url'], $row['_id'], $option['icon'], $option['position'], $option['daley'], $option['tooltip']);
                }
                echo "</td>";
            }
            echo '</tr>';
        }
        if (!empty($colNum)) {
            return $colNum;
        }
    }

    public static function createModelDataTable($model, $url = NULL, $htmlClass = 'bordered', $param_name = NULL) {
        /* Método para gerar as tags de Select para formulários.

         * @param String $name nome do campo;         
         * @param String $id identificador do campo;         
         * @param String $list array de dados para gerar os options do campo;         
         * @param String $action_js método javascript do campo (opcional);         
         */
        require_once "../model/$model.php";
        $object = new $model();
        $lst = $object->getList();
        $colNum = 0;
        echo "<table class='{$htmlClass
        } table dataTable' id='example' cellspacing='0' width='100%'><thead><tr>";
        if (!empty($lst)) {
            echo "<th>nº</th>";
            $colNum += 1;
            foreach ($lst[0] as $key => $value) {
                if ($key[0] != '_') {
                    $colNum += 1;
                    echo "<th>{$object::getDescriptionKey($key)}</th>";
                }
            }
            if (!(empty($url) && empty($param_name))) {
                $colNum += 1;
                echo "<th>Detalhes</th>";
            }
        }
        echo "</tr></thead><tbody>";
        if (!empty($lst)) {
            foreach ($lst as $nrow => $row) {
                echo "<tr> ";
                echo "<td>" . ($nrow + 1) . "</td>";
                foreach ($row as $key => $value) {
                    if ($key[0] != '_') {
                        echo "<td>$value</td>";
                    }
                }
                if (!(empty($url) && empty($param_name))) {
                    echo "<td>";
                    self::createLink($url, $row[$param_name], "assignment", "Detalhes");
                    echo "</td>";
                }
                echo '</tr>';
            }
        } else {
            echo "<tr class=''><td>Sem dados para exibição!</td></tr>";
        }
        echo "</tbody><tfoot><tr>";
        if (!empty($lst)) {
            $colNum -= 1;
            echo "<th colspan=' {
                $colNum
            }'>Total</th>";
            echo "<th>" . count($lst) . "</th>";
        }

        echo "</tr></tfoot></table>";
    }

    public static function createDataTable($lst, $url = NULL, $htmlClass = 'bordered') {
        /* Método para gerar as tags de Select para formulários.

         * @param String $name nome do campo;         
         * @param String $id identificador do campo;         
         * @param String $list array de dados para gerar os options do campo;         
         * @param String $action_js método javascript do campo (opcional);         
         */
        $colNum = 0;
        echo "<table class=' {
                $htmlClass
            } table dataTable' id='example' cellspacing='0' width='100%'><thead><tr>";
        if (!empty($lst)) {
            echo "<th>nº</th>";
            $colNum += 1;
            foreach ($lst[0] as $key => $value) {
                if ($key[0] != '_') {
                    $colNum += 1;
                    echo "<th>{$key}</th>";
                }
            }
        }
        echo "</tr></thead><tbody>";
        if (!empty($lst)) {
            foreach ($lst as $nrow => $row) {
                echo "<tr> ";
                echo "<td>" . ($nrow + 1) . "</td>";
                foreach ($row as $key => $value) {
                    if ($key[0] != '_') {
                        echo "<td>$value</td>";
                    }
                }
                echo '</tr>';
            }
        } else {
            echo "<tr class=''><td>Sem dados para exibição!</td></tr>";
        }
        echo "</tbody><tfoot><tr>";
        if (!empty($lst)) {
            $colNum -= 1;
            echo "<th colspan=' {
                $colNum
            }'>Total</th>";
            echo "<th>" . count($lst) . "</th>";
        }

        echo "</tr></tfoot></table>";
    }

    public static function createFormGroupSelect($title, $properties, $value, $text, $class = NULL, $id = NULL, $idselected = NULL) {
        /* Método para gerar as tags de Select para formulários.

         * @param String $name nome do campo;         
         * @param String $id identificador do campo;         
         * @param String $list array de dados para gerar os options do campo;         
         * @param String $action_js método javascript do campo (opcional);         
         */
        echo '<div  class="' . $class . '" id="' . $id . '">';
        if ($title != NULL) {
            
        }
        Helper::createSelect($properties, $value, $text, $idselected);
        echo "<label>$title</label>";
        echo '</div>';
    }

    public static function createSelect(Array $properties, $value, $text, $idselected = NULL) {
        /* Método para gerar as tags de Select para formulários.

         * @param String $name nome do campo;         
         * @param String $id identificador do campo;         
         * @param String $list array de dados para gerar os options do campo;         
         * @param String $action_js método javascript do campo (opcional);         
         */ echo '<select ';
        foreach ($properties as $row => $val) {
            if ($row != 'list') {
                echo $row . "='" . $val . "' ";
            }
        } echo '>';
        echo '<option value="" selected="selected" />Selecione...</option>';
        foreach ($properties['list'] as $row => $val) {
            if (isset($idselected) && $idselected == $val["$value"]) {
                echo '<option value="' . $val["$value"] . '" selected="selected" />' . $val["$text"] . "</option>";
            } else {
                echo "<option value='" . $val["$value"] . "'/>" . $val["$text"] . "</option>";
            }
        }
        echo "</select>";
    }

    public static function createFormGroupMultSelect($title, $properties, $value, $text, $data_price, $class = NULL, $idselected = NULL) {
        /* Método para gerar as tags de Select para formulários.

         * @param String $name nome do campo;         
         * @param String $id identificador do campo;         
         * @param String $list array de dados para gerar os options do campo;         
         * @param String $action_js método javascript do campo (opcional);         
         */
        echo '<div class="' . $class . '">';
        if ($title != NULL) {
            echo "<label>$title</label>";
        }
        echo '<div>';
        Helper:: createMultSelect($properties, $value, $text, $data_price, $idselected);

        echo '</div>';
        echo '</div>';
    }

    public static function createMultSelect(Array $properties, $value, $text, $data_price, $idselected = NULL) {
        /* Método para gerar as tags de Select para formulários.

         * @param String $name nome do campo;         
         * @param String $id identificador do campo;         
         * @param String $list array de dados para gerar os options do campo;         
         * @param String $action_js método javascript do campo (opcional);         
         */ echo '<s elect ';
        foreach ($properties as $row => $val) {
            if ($row != 'list') {
                echo $row . "='" . $val . "' ";
            }
        } echo '>';
        foreach ($properties['list'] as $row => $val) {
            if (isset($idselected) && $idselected == $val["$value"]) {
                echo '<option value="' . $val["$value"] . '" data-price="' . $val ["$data_price"] . '" selected="selected" />' . $val["$text"] . "</option>";
            } else {
                echo "<option value='" . $val["$value"] . "' data-price='" . $val["$data_price"] . "'/>" . $val["$text"] . "</option>";
            }
        }
        echo "</select>";
    }

    public static function createInputRadio($title, Array $properties1, Array $properties2) {
        /* Método para gerar os inputs para formulários.

         * @param String $type tipo de input;         
         * @param String $name nome do input;         
         * @param String $id identificador do input;         
         * @param String $value Valor do input;         
         * @param String $title titulo do label (opcional);         
         */
        echo "<div class='row-fluid'><div class='col-lg-12'><label>$title</label><div class='col-lg-3'><span>" . $properties1['title'] . "</span>"
        . "<input ";
        foreach ($properties1 as $key => $value) {
            if ($key != 'title' && $key != 'checked') {
                echo ' ' . $key . '="' . $value . '"';
            } elseif ($key == 'checked' && $value == 'checked') {
                echo ' checked="checked"';
            }
        }
        echo "/>"
        . "</div><div class='col-lg-3'>";
        echo "<span>" . $properties2['title'] . "< /span><in put ";
        foreach ($properties2 as $key2 => $value2) {
            if ($key2 != 'title' && $key2 != 'checked') {
                echo ' ' . $key2 . ' = "' . $value2 . '"';
            } elseif ($key2 == 'checked' && $value2 == 'checked') {
                echo ' checked="checked"';
            }
        }

        echo "/></div></div></div>";
    }

    public static function createFormGroupInput($title, Array $properties, $class = NULL, $id = NULL) {
        /* Método para gerar os inputs para formulários.

         * @param String $type tipo de input;         
         * @param String $name nome do input;         
         * @param String $id identificador do input;         
         * @param String $value Valor do input;         
         * @param String $title titulo do label (opcional);         
         */
        echo '<div class="' . $class . '" id="' . $id . '">';
        if ($title != NULL) {
            echo "<label>$title</label>";
        }
        echo ''
        . '<div>';
        Helper::createInput($properties);

        echo '</div>'
        . '</div>';
    }

    public static function createInput(Array $properties) {
        /* Método para gerar os inputs para formulários.

         * @param String $type tipo de input;         
         * @param String $name nome do input;         
         * @param String $id identificador do input;         
         * @param String $value Valor do input;         
         * @param String $title titulo do label (opcional);         
         */
        echo '<input';

        foreach ($properties as $key => $value) {
            echo ' ' . $key . ' = "' . $value .
            '"';
        }
        echo '/>';
    }

    public static function createSubmit($name, $title, $position, $delay, $class = NULL) {
        echo '<div class="btn-group btn-group-vertical">'
        . '<button type="submit" class="' . $class . '" name="' . $name . '" value=""><i> </i>' .
        $title . '</button>'
        . '</div>';
    }

    public static function createLink($url, $id, $icon, $position, $delay, $hint, $class = 'white-text green darken-1') {
        echo '<a href="' . $url . '?id=' . $id . '" class="waves-effect btn-floating secondary-content tooltipped"  data-position="' . $position . '" data-delay="' . $delay . '" data-tooltip="' . $hint . '"><i class="material-icons  ' . $class . '">' . $icon . '</i></a>';
    }

    public static function createFormGroupCheckbox($title, Array $properties, $class = NULL, $id = NULL) {
        /* Método para gerar os inputs para formulários.

         * @param String $type tipo de input;         
         * @param String $name nome do input;         
         * @param String $id identificador do input;         
         * @param String $value Valor do input;         
         * @param String $title titulo do label (opcional);         
         */
        echo '<div class="' . $class . '" id="' . $id . '">';
        Helper::createCheckBox($properties);
        if ($title != NULL) {
            echo "<label>$title

</label>";
        }
        echo '</div>';
    }

    public static function createCheckBox(Array $properties) {
        /* Método para gerar os inputs para formulários.

         * @param String $type tipo de input;         
         * @param String $name nome do input;         
         * @param String $id identificador do input;         
         * @param String $value Valor do input;         
         * @param String $title titulo do label (opcional);         
         */ echo '<input ';

        foreach ($properties as $key => $value) {
            if ($key == 'value' && $value == '1') {
                echo 'checked="checked" ' . $key . ' = "' . $value . '"';
            } else {
                echo ' ' . $key . ' = "' . $value . '"';
            }
        }
        echo '/>';
    }

    public static function formatDateTime($datetime, $format = 'd/m/Y H:i') {
        /* Método para formatar a data de acordo com a necessidade para exibi-la.

         */
        if (!empty($datetime)) {
            $date = date("$format", strtotime($datetime));
            return $date;
        } else {
            echo '';
        }
    }

    public static function formatNameToShort($name, $maxLength = 15, $words = 1) {
        /* Método para formatar a data de acordo com a necessidade para exibi-la.
         */
        $short = explode(' ', $name)[0];
        if (count($short) > $maxLength) {
            return substr(count($short), $maxLength);
        } else {
            return $short;
        }
    }

}
