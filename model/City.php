<?php

require_once '../model/State.php';
require_once '../model/Crud.php';

class City extends Crud {

    const PLURAL_DESC = "Cidades";
    const SINGULAR_DESC = "Cidade";
    const PATO_BRANCO_ID = 3042;

    protected $id_city;
    protected $name;
    protected $id_state;
    protected $_uf;
    protected $_state;
    protected $_table = "CITY";
    protected $_list = 'lst_city';
    protected $_key_field = 'id_city';

    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    function __get($atribute) {
        return $this->$atribute;
    }

    function __construct($idcity = NULL) {
        parent::__construct();
        if ($idcity > 0) {
            $this->find($idcity);
        }
    }
    static public function getDescriptionKey($key) {
        try {
            switch ($key) {
                case 'id_city':
                    return 'ID';
                case 'name':
                    return 'Nome';
                case 'id_state':
                    return 'Estado Nº';
            }
        } catch (Exception $exc) {
            return NULL;
        }
    }

    public function find($id) {
        if (parent::find(Array($this->_key_field => $id))) {
            $this->_state = new State($this->id_state);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getState() {
        $state = new State();

        $state->find(array("id_state" => $this->id_state));

        return $state;
    }

    public function getStateCities($idstate) {
        try {
            $select = "SELECT * FROM `{$this->_table}` WHERE id_state = $idstate ORDER BY `name` ASC "; //atribuindo a string do sql
            $stmt = $this->_db->prepare("$select"); //preparando o comando PDO
            if ($stmt->execute()) { //executando a resposta
                $stmt->setFetchMode(PDO::FETCH_ASSOC); //realizando o fetchmode na resposta
                return $stmt->fetchAll(); //transformando em array a resposta e retornando-a
            } else {
                Logger::log_array(__CLASS__, $stmt->errorInfo());
            }
        } catch (PDOExecption $e) { //verificando erros
            Logger::log_array(__CLASS__, $e);
            throw $e;
        }
    }

}

?>
