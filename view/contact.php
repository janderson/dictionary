<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?= Config::TITLE ?></title>
        <script src="../view/js/jquery.validate.min.js" type="text/javascript" language="javascript"></script>
        <script src="../view/js/jquery.maskedinput.js" type="text/javascript" language="javascript"></script>
    </head>
    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main>
            <div class="section">
                <div class="container">
                    <div class="row">
                        <div class="col s12 l8 offset-l2">
                            <?php Helper::messageBoxRow(); ?>
                        </div>
                        <div class="col s10 offset-s1">
                            <div class="card">
                                <div class="card-content green darken-2 center-align">
                                    <div class="white-text card-title">Entre em contato conosco</div>
                                </div>
                                <form id="form-contact" class="card-content form-horizontal" method="POST" action="../controller/ContactController.php">
                                    <div class="row">
                                        <div class="input-field col s12 l6">
                                            <input id="name" name="name" type="text" class="validate">
                                            <label for="name">Nome</label>
                                        </div>
                                        <div class="input-field col s12 l6">
                                            <input id="email" type="email" name="email" class="validate">
                                            <label for="email">E-mail</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 l12">
                                            <input id="subject" type="text" name="subject" class="validate">
                                            <label for="subject" data-error="wrong" data-success="right">Assunto</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 l12">
                                            <textarea id="textarea1" class="materialize-textarea" name="content"></textarea>
                                            <label for="textarea1">Mensagem</label>
                                        </div>
                                    </div>
                                    <div class="center-align">
                                        <?php
                                        echo "<input name='id_user' value='" . (!empty($current_user) && $current_user instanceof User ? $current_user->id_user : '') . "' type='hidden' />";
                                        ?>
                                        <button name="option" value="send" class="btn button-margin waves-effect waves-light darken-1 green" data-operation="register">ENVIAR MENSAGEM</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <script src="../view/js/form-validation-script.js" type="text/javascript" language="javascript"></script>
        <?php include '../view/partial/scripts.php'; ?>
    </body>
</html>
