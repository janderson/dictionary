<?php
include '../view/partial/helper.php';
Helper::bootSession();
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title>INVENTUM 2015</title>
    </head>
    <body>
        <?php include '../view/partial/header.php'; ?>
        <div class="container">
            <div class="section">
                <?php
                Helper::messageBoxRow(FALSE);
                ?>               
                <div class="row">
                    <a href="../view/login.php" class="waves-effect waves-light btn">Login</a>
                    <a href="../view/contact.php" class="waves-effect orange darken-2 waves-light btn">Contato</a>
                </div>
            </div>
        </div>
        <?php include '../view/partial/scripts.php'; ?>
        <script src="../view/js/facebook.js"></script>
    </body>
</html>