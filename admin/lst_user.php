<!DOCTYPE html>
<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
Helper::requireAdminSession($current_user);
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?= Config::TITLE ?></title>
        <link href="../library/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    </head>

    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main>
            <div class="row">
                <section class="col s0 l3">
                    <?php include '../view/partial/adminmenu.php'; ?>
                </section>
                <section class="col s12 l9">
                    <!-- Teal page content  -->
                    <?php Helper::messageBoxRow(); ?>
                    <div class="row">
                        <div class="card">
                            <div class="card-content">
                                <?php
                                    $options = Array(
                                        Array("url" => './dtl_user.php', "icon" => "mode_edit", "position" => "left", "daley" => "50", "tooltip" => "Editar")
                                    );
                                    Helper::createTableNovo(array("model" => "User", "list" => '', "options" => $options, "html_class" => "striped", "line_number" => FALSE));
                                ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </main>
        <?php include '../view/partial/scripts.php'; ?>
        <script type="text/javascript" src="../library/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" class="init">

            $(document).ready(function () {
                $('#example').dataTable();
            });
        </script>
    </body>
</html>
