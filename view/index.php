<?php
include './partial/helper.php';
$current_user = Helper::bootSession();
require_once '../model/Word.php';
include_once '../model/Language.php';
?>
<html>
    <head>
        <?php include './partial/head.php'; ?>
        <title><?php echo Config::TITLE ?></title>
    </head>
    <body>
        <header>
            <?php include './partial/header.php'; ?>
        </header>
        <main class="section">
            <?php
            Helper::messageBoxRow();
            ?>
            <div class="row">
                <?php
                if (empty($current_user)) {
                    include './partial/login.php';
                } else {
                    ?>
                    <div class="col s12 l3 hide-on-med-and-down">
                        <?php include './partial/usermenu.php'; ?>
                    </div>
                    <?php
                }
                ?>
                <div class="col s12 l9">
                    <div class="row">
                        <div class="card card-research">
                            <div class="card-content">
                                <div class="col s12 l8 offset-l3">
                                    <div class="section">
                                        <form action="../controller/WordController.php" method="POST" id="form-research">
                                            <div class="row valign-wrapper">
                                                <div class="card center-align">
                                                    <div class="card-content green lighten-2">
                                                        <div class="card-title center-align">Pesquise sua palavra aqui.</div>
                                                    </div>
                                                    <div class="card-content">
                                                        <div class="input-field col s12 l4 valign">
                                                            <input type="text" class="validate" placeholder="Pesquisar" name="word_research" />
                                                        </div>
                                                        <div class="input-field col s12 l4 valign">
                                                            <?php
                                                            $language = new Language();
                                                            $list = $language->read();
                                                            Helper::createSelect(array('list' => $list,
                                                                'id' => 'language',
                                                                'name' => 'id_language',
                                                                'class' => 'id_language browser-default'), 'id_language', 'name', NULL);
                                                            ?>
                                                        </div>
                                                        <div class="input-field col s12 l4 valign">
                                                            <input type="hidden" class="btn green darken-2" name="option" value="research_word" />
                                                            <input type="submit" class=" col s12 btn green darken-2" value="PESQUISAR" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class=" row section">
                                    <div class="col s12 l4 offset-l1">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-title grey-text text-darken-4 center-align">FUTURO JOGO </div>
                                            </div>
                                            <div class="card-action center-align">
                                                <a class="btn green darken-4 white-text" href="#">ENTRAR</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col s12 l4 offset-l2">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-title grey-text text-darken-4 center-align">TESTE RECOMENDADO</div>
                                            </div>
                                            <div class="card-action center-align">
                                                <a class="btn green darken-4 white-text" href="#">ENTRAR</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php include './partial/footer.php'; ?>
        <?php include './partial/scripts.php'; ?>
    </body>
</html>