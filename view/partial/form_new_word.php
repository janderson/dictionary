<?php
$language = new Language();
$class = new GramaticalClass();
$list_language = $language->read();
$list_class = $class->read();
?>
<form class="row margin-none" id="form-new-word" action="../controller/WordController.php" method="POST">
    <div class="col s12 center-align grey lighten-3">
        Obs.: Caso não conheça a tradução para a palavra deixe em branco, nossos colaboradores farão a tradução da mesma.
    </div>
    <div class="col s12 m6 l3">
        <div class="row">
            <div class="col s12">
                <label>Idioma</label>
                <?php
                Helper::createSelect(array('list' => $list_language, 'id' => 'id_first_language', 'name' => 'language[0]', 'class' => 'id_first_language browser-default'), 'id_language', 'name', NULL);
                ?>
            </div>
            <div class="input-field col s12 l6">
                <input placeholder="*Obrigatório" id="first_word" type="text" name="word[0]" class="validate">
                <label for="first_word">Palavra</label>
            </div>
            <div class="input-field col s12 l6">
                <input placeholder="Opcional" id="first_plural" type="text" name="plural[0]" class="validate">
                <label for="first_plural">Plural</label>
            </div>
            <div class="input-field col s12 l6">
                <input placeholder="Opcional" id="first_gramatical_class" type="text" name="gramatical_class[0]" class="validate">
                <label for="first_gramatical_class">Cl. Gram.</label>
            </div>
            <div class="input-field col s12 l6">
                <input placeholder="Opcional" id="category" type="text" name="category[0]" class="validate">
                <label for="first_category">Categorias</label>
            </div>
            <div class="input-field col s12">
                <input placeholder="Opcional" type="text" name="phrase_example[0]" id="phrase_example" class="validate">
                <label for="phrase_example_first_language">Frase de Exemplo no primeiro idioma</label>
            </div>
        </div>
    </div>
    <div class="col s12 m6 l3">
        <h5 class="curious-blue-text center-align translate1">Tradução 1</h5>
        <div class="row">   
            <input type="hidden" name="language[1]" id="language1" value="" />
            <div class="input-field col s12 l6">
                <input placeholder="Opcional" type="text" class="validate" id="second_word" name="word[1]">
                <label for="second_word">Tradução</label>
            </div>
            <div class="input-field col s12 l6">
                <input placeholder="Opcional" type="text" class="validate" id="second_plural" name="plural[1]">
                <label for="second_plural">Plural</label>
            </div>
            <div class="input-field col s12 l6">
                <input placeholder="Opcional" id="second_gramatical_class" type="text" name="gramatical_class[1]" class="validate">
                <label for="second_gramatical_class">Cl. Gram.</label>
            </div>
            <input type="hidden" name="category[1]" id="category1" value="" />
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input placeholder="Opcional" type="text" name="phrase_example[1]" id="phrase_example" class="validate">
                <label for="phrase_example_second_language">Frase de Exemplo no segundo idioma</label>
            </div>
        </div>
    </div>
    <div class="col s12 m6 l3">
        <h5 class="curious-blue-text center-align translate2">Tradução 2</h5>
        <div class="row">   
            <input type="hidden" name="language[2]" id="language2" value="" />
            <div class="input-field col s12 l6">
                <input placeholder="Opcional" type="text" class="validate" id="second_word" name="word[2]">
                <label for="second_word">Tradução</label>
            </div>
            <div class="input-field col s12 l6">
                <input placeholder="Opcional" type="text" class="validate" id="second_plural" name="plural[2]">
                <label for="second_plural">Plural</label>
            </div>
            <div class="input-field col s12 l6">
                <input placeholder="Opcional" id="second_gramatical_class" type="text" name="gramatical_class[2]" class="validate">
                <label for="second_gramatical_class">Cl. Gram.</label>
            </div>
            <input type="hidden" name="category[2]" id="category2" value="" />
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input placeholder="Opcional" type="text" name="phrase_example[2]" id="phrase_example" class="validate">
                <label for="phrase_example_second_language">Frase de Exemplo no segundo idioma</label>
            </div>
        </div>
    </div>
    <div class="col s12 m6 l3">
        <h5 class="curious-blue-text center-align translate3">Tradução 3</h5>
        <div class="row">   
            <input type="hidden" name="language[3]" id="language3" value="" />
            <div class="input-field col s12 l6">
                <input placeholder="Opcional" type="text" class="validate" id="second_word" name="word[3]">
                <label for="second_word">Tradução</label>
            </div>
            <div class="input-field col s12 l6">
                <input placeholder="Opcional" type="text" class="validate" id="second_plural" name="plural[3]">
                <label for="second_plural">Plural</label>
            </div>
            <div class="input-field col s12 l6">
                <input placeholder="Opcional" id="second_gramatical_class" type="text" name="gramatical_class[3]" class="validate">
                <label for="second_gramatical_class">Cl. Gram.</label>
            </div>
            <input type="hidden" name="category[3]" id="category3" value="" />
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input placeholder="Opcional" type="text" name="phrase_example[3]" id="phrase_example" class="validate">
                <label for="phrase_example_second_language">Frase de Exemplo no segundo idioma</label>
            </div>
        </div>
    </div>
    <input type="hidden" name="redirect" value="<?= $_SERVER['PHP_SELF'] ?>" />
    <input type="hidden" name="id_user" value="<?= $current_user->id_user ?>" />
    <input type="hidden" name="option" value="new_translate" />
</form>