<div class="row-fluid content">
    <?php
    if (isset($_SESSION['error_code'])) {
        ?>
        <div class="alert alert-danger alert-dismissible fade in col-lg-12 error-box">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong class="col-lg-3 col-sm-4"><i class="glyphicon glyphicon-alert"></i> Ops... </strong>
            <span class="col-lg-8 col-sm-6">
                <?php
                echo $_SESSION['error_message'];
                $code = $_SESSION['error_code'];
                if ($_SESSION['error_code'] == '1103' || $_SESSION['error_code'] ==  1220) {
                    echo '<a href="../view/buy_package.php" class="btn btn-primary btn-buy"> <span class="glyphicon glyphicon-shopping-cart"></span>   Comprar Créditos</a>';
                }
                ?>
            </span>
        </div>
        <?php
    } elseif (isset($_SESSION['sucess'])) {
        ?>
        <div class="alert alert-success alert-dismissible fade in col-lg-12 col-sm-offset-0 error-box ">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong class="col-lg-3 col-sm-4"><i class="glyphicon glyphicon-ok"></i> Sucesso! </strong>
            <span class="col-lg-8 col-sm-6">
                <?php
                echo $_SESSION['sucess'];
                ?>
            </span>
        </div>
        <?php
    }
    unset($_SESSION['sucess']);
    unset($_SESSION['error_code']);
    unset($_SESSION['error_message']);
    ?>
</div>