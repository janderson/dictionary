<!DOCTYPE html>
<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
Helper::requireAdminSession($current_user);
if (empty($_GET['id']) || !$_GET['id'] > 0) {
    header('location: ' . Helper::handleError('5010'));
    exit();
}
$admin = new Admin($_GET['id']);
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?= Config::TITLE ?></title>
    </head>

    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main>
            <div class="row">
                <section class="col s0 l3">
                    <?php include '../view/partial/adminmenu.php'; ?>
                </section>
                <section class="col s12 l9">
                    <!-- Teal page content  -->
                    <?php Helper::messageBoxRow(); ?>
                    <div class="row">
                        <div class="card">
                            <div class="card-content">
                                <form class="col s12" action="../controller/AdminController.php" method="POST" id="form-admin">
                                    <?php Helper::createInput(array('type' => 'hidden', 'value' => $admin->id_admin, 'name' => 'id_admin')); ?>
                                    <h5 class="header center blue-text">Editar Administrador</h5>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input placeholder="Nome (obrigatório)" id="name" type="text" name="name" class="validate" value="<?= $admin->name ?>">
                                            <label for="name">Nome</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 l8">
                                            <input placeholder="Informe um e-mail válido (obrigatório)" id="email" type="email" name="email" value="<?= $admin->email ?>" class="validate" disabled="">
                                            <label for="email">Email</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 l6">
                                            <input placeholder="Informe uma senha " id="password" type="password" name="password" >
                                            <label for="password">Senha</label>
                                        </div>
                                        <div class="input-field col s12 l6">
                                            <input placeholder="Repita a senha " id="re-password" type="password" name="re_password" >
                                            <label for="re-password">Repita a Senha digitada</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m6 l4">
                                            <input type="checkbox" class="filled-in" id="user_control" name="user_control" <?= (!empty($admin->user_control) ? 'checked' : '') ?> />
                                            <label for="user_control">Controle de Usuários</label>
                                        </div>
                                        <div class="input-field col s12 m6 l4">
                                            <input type="checkbox" class="filled-in" id="admin_control" name="admin_control" <?= (!empty($admin->admin_control) ? 'checked' : '') ?> />
                                            <label for="admin_control">Controle de Administradores</label>
                                        </div>
                                        <div class="input-field col s12 m6 l4">
                                            <input type="checkbox" class="filled-in" id="words_control" name="words_control" <?= (!empty($admin->words_control) ? 'checked' : '') ?> />
                                            <label for="words_control">Controle de Palavras</label>
                                        </div>
                                        <div class="input-field col s12 m6 l4">
                                            <input type="checkbox" class="filled-in" id="gramatical_control" name="gramatical_control" <?= (!empty($admin->gramatical_control) ? 'checked' : '') ?> />
                                            <label for="gramatical_control">Controle das Classes Gramaticais</label>
                                        </div>
                                        <div class="input-field col s12 m6 l4">
                                            <input type="checkbox" class="filled-in" id="translation_control" name="translation_control" <?= (!empty($admin->translation_control) ? 'checked' : '') ?> />
                                            <label for="translation_control">Controle de Traduções</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button type="submit" name="option" value="edt_admin" class="waves-effect waves-light btn">Enviar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </main>
        <?php include '../view/partial/scripts.php'; ?>
        <script src="../view/js/admin-forms-validation.js"></script>
        <script>
            $("#cpf").mask("999.999.999-99");
            $("#phone").mask("(99)9999-9999?9");
            $("#cellphone").mask("(99)9999-9999?9");
        </script>
    </body>
</html>
