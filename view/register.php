<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?php echo Config::TITLE ?></title>
    </head>
    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main>
            <div class="section">
                <?php
                include_once '../model/State.php';
                include_once '../model/City.php';
                $city = new City(City::PATO_BRANCO_ID);
                $state = new State($city->id_state);
                $list_state = $state->read();

                $list_city = $city->getStateCities($state->id_state);
                Helper::messageBoxRow();
                ?>
                <div class="row">
                    <?php
                    include './partial/login.php';
                    ?>
                    <form class="col s12 l9" action="../controller/UserController.php" method="POST" name="register" id="form-register">
                        <div class="card">
                            <div class="card-content">
                                <h4 class="header center blue-text">Cadastre-se</h4>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input placeholder="Digite seu nome (obrigatório)" id="name" type="text" name="name" class="validate">
                                        <label for="name">Nome</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 l8">
                                        <input placeholder="Informe um e-mail válido" id="email" type="email" name="email" class="validate">
                                        <label for="email">Email</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 l6">
                                        <input placeholder="Informe uma senha (obrigatório)" id="password" type="password" name="password" class="validate">
                                        <label for="password">Senha</label>
                                    </div>
                                    <div class="input-field col s12 l6">
                                        <input placeholder="Repita sua senha (obrigatório)" id="re-password" type="password" name="re_password" class="validate">
                                        <label for="re-password">Repita sua Senha</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12 l6">
                                        <label class="left-align col s12">Estado</label>
                                        <div class="input-field col s12">
                                            <select id="id_state" class="browser-default">
                                                <?php
                                                foreach ($list_state as $row => $ob_state) {
                                                    if ($ob_state['id_state'] == $state->id_state) {
                                                        echo "<option value={$ob_state['id_state']} selected='selected'>{$ob_state['name']}</option>";
                                                    } else {
                                                        echo "<option value={$ob_state['id_state']}>{$ob_state['name']}</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="load_cities" class="col s12 l6">
                                        <label class="left-align col s12">Cidade</label>
                                        <div class="input-field col s12" >
                                            <select name = 'id_city' id = 'combobox_city' class='browser-default validate'>
                                                <?php
                                                foreach ($list_city as $row => $ob_city) {
                                                    if ($ob_city['id_city'] == $city->id_city) {
                                                        echo "<option value={$ob_city['id_city']} selected='selected'>{$ob_city['name']}</option>";
                                                    } else {
                                                        echo "<option value={$ob_city['id_city']}>{$ob_city['name']}</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <button type="submit" name="option" value="new_user" class="btn button-margin waves-effect waves-light ">Enviar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </main>
        <?php include '../view/partial/scripts.php'; ?>
        <script>
            jQuery('#dt_born').datetimepicker({
                timepicker: false,
                format: 'd/m/Y',
                minDate: '01/01/1900',
                maxDate: 0
            });
            $("#cpf").mask("999.999.999-99");
            $("#dt_born").mask("99/99/9999");
        </script>
        <script>
            $(document).ready(function () {
                $('#id_state').change(function () {
                    var state = $('#id_state').val();
                    if (state) {
                        var url = '../controller/HelperController.php?idstate=' + state;
                        $.get(url, function (dataReturn) {
                            $('#load_cities').html(dataReturn);
                        });
                    }
                });
            });
        </script>
    </body>
</html>