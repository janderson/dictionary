<li class="spacing center-align"> '
    <img src="https://graph.facebook.com/picture?width=120&height=120" class="circle"/>
    </li>
<li>
    <a class="valign-wrapper " href="../view/profile.php">
        <i class='material-icons tiny circle  hide-on-med-and-down'>perm_identity</i> Seu perfil</a>
</li>
<li>
    <a class="valign-wrapper"  href="../view/control_panel.php">
        <i class='material-icons tiny circle  hide-on-med-and-down'>assignment</i> Painel de controle</a>
</li>
<li>
    <a class="valign-wrapper"  href="../view/new_word.php">
        <i class='material-icons tiny circle  hide-on-med-and-down'>assignment</i> Inserir palavra</a>
</li>
<li>
    <a class="valign-wrapper"  href="../view/find_word.php">
        <i class='material-icons tiny circle  hide-on-med-and-down'>assignment</i> Pesquisar</a>
</li>
<li>
    <a class="valign-wrapper"  href="../view/lst_categories.php">
        <i class='material-icons tiny circle  hide-on-med-and-down'>assignment</i> Categorias</a>
</li>
<li>
    <a class="valign-wrapper"  href="../view/suggestions.php">
        <i class='material-icons tiny circle  hide-on-med-and-down'>assignment</i> Sugestões</a>
</li>
<li>
    <a class="valign-wrapper"  href="../controller/UserController.php?option=logout">
        <i class='material-icons tiny circle  hide-on-med-and-down'>power_settings_new</i> Sair</a>
</li>