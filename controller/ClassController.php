<?php

require_once './Controller.php';
require_once './ExceptionController.php';
require_once '../model/GramaticalClass.php';

class ClassController extends Controller {

    const ERROR_CODE_PREFIX = '10';
    const CHANGE_PASSWORD = "<a href='http://localhost/frade/admin/req_new_password.php";
    const URL_CONTROL_PANEL = "../admin/control_panel.php";

    private $class;

    public function __construct() {
        parent::__construct();
        try {
            $this->class = new GramaticalClass();
            switch ($_SERVER['REQUEST_METHOD']) {
                case "POST":
                    switch ($_POST['option']) {
                        case 'new_class':
                            $this->newClass();
                            break;
                        case 'edt_class':
                            $this->edtClass();
                            break;
                        default:
                            break;
                    }
                    exit();
                case "GET":
                    var_dump($_GET);
                    exit();
                default :
                    throw new ExceptionController('Método não suportado!', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_OPTION, NULL);
            }
        } catch (Exception $e) {
            if ($e instanceof ExceptionController || $e instanceof CrudException) {
                $_SESSION['error_code'] = $e->getCode();
                $_SESSION['error_message'] = $e->getMessage();
                if (empty($_SERVER['HTTP_REFERER'])) {
                    header("Location: " . ExceptionController::PATH_ERROR_PAGE);
                } else {
                    header("Location: {$_SERVER['HTTP_REFERER']}");
                }
            } else {
                ExceptionController::handleException($e);
            }
        }
    }

    private function newClass() {
        $this->requireAdminSession();
        $this->class->name = $_POST['name'];
        $this->class->description = $_POST['description'];

        if (!$this->class->create(array('id_class' => $this->class->id_class)) > 0) {
            throw new ExceptionController('Falha ao Cadastrar', self::ERROR_CODE_PREFIX . ExceptionController::FAIL_REGISTER);
        } else {
            $_SESSION['sucess'] = 'Cadastrado com sucesso.';
            header("Location: ../admin/lst_gramatical_class.php");
        }
    }

    private function edtClass() {
        $this->requireAdminSession(Array("words_control"));
        if (empty($_POST['id_class']) || !$this->class->find($_POST['id_class'])) {
            throw new ExceptionController('Administrador inválido', self::ERROR_CODE_PREFIX . '54');
        }
        $this->class->name = $_POST['name'];
        $this->class->description = $_POST['description'];

        if (!$this->class->update(array('id_class' => $this->class->id_class)) > 0) {
            throw new ExceptionController('Não foi possível atualizar o idioma', self::ERROR_CODE_PREFIX . ExceptionController::FAIL_REGISTER);
        } else {
            $_SESSION['sucess'] = 'Classe Gramatical atualizado.';
            header("Location: ../admin/lst_gramatical_class.php");
        }
    }
}
new ClassController();
