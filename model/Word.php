<?php

require_once("Crud.php");
require_once("User.php");
require_once("Category.php");

class Word extends Crud {

    protected $id_word;
    protected $word;
    protected $plural;
    protected $id_user;
    protected $_user;
    protected $id_category;
    protected $_category;
    protected $_dtt_register;
    protected $_table = 'WORD';
    protected $_list = 'lst_word';
    protected $_key_field = 'id_word';

    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    function __get($atribute) {
        return $this->$atribute;
    }

    function __construct($idword = NULL) {
        parent::__construct();
        if ($idword > 0) {
            $this->find($idword);
        }
    }

    public function find($id) {
        if (parent::find(Array($this->_key_field => $id))) {
            $this->_user = new User($this->id_user);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getSerialized($id = NULL) {
        $this->retriveAll(($id != NULL ? Array($this->_key_field => $id) : ''));
        $serialized = serialize($this);
        return $serialized;
    }

    public function __sleep() {
        return array('id_word', 'word', 'id_user', 'plural', 'id_category');
    }

    public function __wakeup() {
        return $this;
    }

    public function getList($rows = 50) {
        try {
            $stmt = $this->_db->prepare("SELECT * FROM get_words");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }

    public function getListFilterByLetter($filter = '') {
        try {
            $stmt = $this->_db->prepare("SELECT * FROM get_words_by_filter " . (!empty($filter) ? "WHERE LEFT(`word`, 1) = '" . $filter . "'" : ''));
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }

    public function listWordsResearch($filter = '', $id_language = NULL) {
        try {
            $stmt = $this->_db->prepare("SELECT * FROM get_words_by_filter WHERE word LIKE '%" . (!empty($filter) ? $filter : '') . "%' " . (!empty($id_language) ? "AND _id_language = " . $id_language : ''));
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }

    public function getWordUntranslated($rows = 50) {
        try {
            $stmt = $this->_db->prepare("SELECT * FROM get_word_untranslated");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }

    public function getWordRegistedToday() {
        try {
            $stmt = $this->_db->prepare("SELECT * FROM `WORD` WHERE DATE_FORMAT(dtt_register, '%Y-%m-%d') = CURDATE()");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return count($stmt->fetchAll());
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }

    static public function getDescriptionKey($key) {
        try {
            switch ($key) {
                case 'id_word':return 'ID';
                case '_id':return 'ID';
                case 'word':return 'Palavra';
                case 'id_user':return 'Usuário';
                case 'plural':return 'Plural';
                case 'id_category':return 'ID Categoria';
                case 'id_language':return 'ID Idioma';
                case 'name_language':return 'Idioma';
                case 'example_sentence':return 'Frase de Exemplo';
                case 'name_class':return 'Classe Gramatical';
                default : return $key;
            }
        } catch (Exception $exc) {
            return NULL;
        }
    }

}
