<?php

require_once './Controller.php';
require_once './ExceptionController.php';
require_once '../model/Language.php';

class LanguageController extends Controller {

    const ERROR_CODE_PREFIX = '10';
    const CHANGE_PASSWORD = "<a href='http://localhost/frade/admin/req_new_password.php";
    const URL_CONTROL_PANEL = "../admin/control_panel.php";

    private $language;

    public function __construct() {
        parent::__construct();
        try {
            $this->language = new Language();
            switch ($_SERVER['REQUEST_METHOD']) {
                case "POST":
                    switch ($_POST['option']) {
                        case 'new_language':
                            $this->newLanguage();
                            break;
                        case 'edt_language':
                            $this->edtLanguage();
                            break;
                        default:
                            break;
                    }
                    exit();
                case "GET":
                    var_dump($_GET);
                    exit();
                default :
                    throw new ExceptionController('Método não suportado!', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_OPTION, NULL);
            }
        } catch (Exception $e) {
            if ($e instanceof ExceptionController || $e instanceof CrudException) {
                $_SESSION['error_code'] = $e->getCode();
                $_SESSION['error_message'] = $e->getMessage();
                if (empty($_SERVER['HTTP_REFERER'])) {
                    header("Location: " . ExceptionController::PATH_ERROR_PAGE);
                } else {
                    header("Location: {$_SERVER['HTTP_REFERER']}");
                }
            } else {
                ExceptionController::handleException($e);
            }
        }
    }

    private function newLanguage() {
        $this->requireAdminSession(Array("words_control"));
        $this->language->name = $_POST['name'];
        $this->language->home_country = $_POST['home_country'];
        $this->language->description = $_POST['description'];

        if (!$this->language->create(array('id_language' => $this->language->id_language)) > 0) {
            throw new ExceptionController('Falha ao Cadastrar', self::ERROR_CODE_PREFIX . ExceptionController::FAIL_REGISTER);
        } else {
            $_SESSION['sucess'] = 'Cadastrado com sucesso.';
            header("Location: ../admin/lst_language.php");
        }
    }

    private function edtLanguage() {
        $this->requireAdminSession(Array("words_control"));
        if (empty($_POST['id_language']) || !$this->language->find($_POST['id_language'])) {
            throw new ExceptionController('Administrador inválido', self::ERROR_CODE_PREFIX . '54');
        }
        $this->language->name = $_POST['name'];
        $this->language->home_country = $_POST['home_country'];
        $this->language->description = $_POST['description'];

        if (!$this->language->update(array('id_language' => $this->language->id_language)) > 0) {
            throw new ExceptionController('Não foi possível atualizar o idioma', self::ERROR_CODE_PREFIX . ExceptionController::FAIL_REGISTER);
        } else {
            $_SESSION['sucess'] = 'Idioma atualizado.';
            header("Location: ../admin/lst_language.php");
        }
    }
}

new LanguageController();
