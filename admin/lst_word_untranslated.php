<!DOCTYPE html>
<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
Helper::requireAdminSession($current_user);
$url_translated = '../admin/new_translate.php';
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?= Config::TITLE ?></title>
    </head>

    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main>
            <div class="row">
                <section class="col s0 l3">
                    <?php include '../view/partial/adminmenu.php'; ?>
                </section>
                <section class="col s12 l9">
                    <!-- Teal page content  -->
                    <?php Helper::messageBoxRow(); ?>
                    <div class="row">
                        <div class="card">
                            <div class="card-content">
                                <?php
                                require_once '../model/Word.php';
                                $word = new Word();
                                $lst = $word->getWordUntranslated();
                                $options = Array(
                                    Array("url" => $url_translated, "icon" => "mode_edit", "position" => "left", "daley" => "50", "tooltip" => "Editar")
                                );
                                Helper::createTableNovo(array("model" => "Word", "list" => $lst, "options" => $options, "html_class" => "striped", "line_number" => FALSE));
                                ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </main>
        <?php include '../view/partial/scripts.php'; ?>
    </body>
</html>
