<!DOCTYPE html>
<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
Helper::requireUserSession($current_user);
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?php echo Config::TITLE ?></title>
    </head>
    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main>
            <div class="container">
                <div class="row">
                    <div class="row">
                        <?php Helper::messageBoxRow(); ?>                                         
                    </div>
                    <div class="col s12 l3 hide-on-med-and-down">
                        <?php include './partial/usermenu.php'; ?>
                    </div>
                    <div class="col s12 l9">
                        <div class="row">
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php include '../view/partial/scripts.php'; ?>
    </body>
</html>
