<?php

require_once './Controller.php';
require_once './ExceptionController.php';

class AdminController extends Controller {

    const ERROR_CODE_PREFIX = '10';
    const CHANGE_PASSWORD = "<a href='http://localhost/frade/admin/req_new_password.php";
    const URL_CONTROL_PANEL = "../admin/control_panel.php";

    private $admin;

    public function __construct() {
        parent::__construct();
        try {
            $this->admin = new Admin();
            switch ($_SERVER['REQUEST_METHOD']) {
                case "POST":
                    switch ($_POST['option']) {
                        case 'login':
                            $login = $_POST['login'];
                            $password = md5($_POST['password']);
                            $this->login($login, $password);
                            break;
                        case 'new_admin':
                            $this->newAdmin();
                            break;
                        case 'edt_admin':
                            var_dump($_POST);
                            $this->editAdmin();
                            break;
                        default:
                            break;
                    }
                    exit();
                case "GET":
                    var_dump($_GET);
                    exit();
                default :
                    throw new ExceptionController('Método não suportado!', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_OPTION, NULL);
            }
        } catch (Exception $e) {
            if ($e instanceof ExceptionController || $e instanceof CrudException) {
                $_SESSION['error_code'] = $e->getCode();
                $_SESSION['error_message'] = $e->getMessage();
                if (empty($_SERVER['HTTP_REFERER'])) {
                    header("Location: " . ExceptionController::PATH_ERROR_PAGE);
                } else {
                    header("Location: {$_SERVER['HTTP_REFERER']}");
                }
            } else {
                ExceptionController::handleException($e);
            }
        }
    }

    private function validatePassword() {
        if ($this->admin->id_admin > 0) {
            if (!empty($_POST['new_password'])) {
                if (md5($_POST['new_password']) == md5($_POST['re_password'])) {
                    $this->admin->password = md5($_POST['new_password']);
                } else {
                    throw new ExceptionController('As senhas digitadas não correspondem', self::ERROR_CODE_PREFIX . '06');
                }
            } else {
                $this->admin->password = $this->admin->password;
            }
        } elseif (empty($_POST['password'])) {
            throw new ExceptionController('Insira uma senha', self::ERROR_CODE_PREFIX . '06');
        } else {
            if (md5($_POST['password']) == md5($_POST['re_password'])) {
                $this->admin->password = md5($_POST['password']);
            } else {
                throw new ExceptionController('As senhas digitadas não correspondem', self::ERROR_CODE_PREFIX . '06');
            }
        }
    }

    private function newAdmin() {
        $this->requireAdminSession(Array("admin_control"));
        $this->admin->name = $this->validateName($_POST['name']);
        $this->admin->email = $this->validateEmail($_POST['email']);
        $this->admin->password = md5(Validate::string($_POST['password'], 4));
        $this->admin->user_control = (isset($_POST['user_control']) ? 1 : 0);
        $this->admin->admin_control = (isset($_POST['admin_control']) ? 1 : 0);
        $this->admin->words_control = (isset($_POST['words_control']) ? 1 : 0);
        $this->admin->translation_control = (isset($_POST['translation_control']) ? 1 : 0);
        $this->admin->gramatical_control = (isset($_POST['gramatical_control']) ? 1 : 0);
        $this->admin->active = 1;

        if (!$this->admin->create(array('id_admin' => $this->admin->id_admin)) > 0) {
            throw new ExceptionController('Falha ao Cadastrar', self::ERROR_CODE_PREFIX . ExceptionController::FAIL_REGISTER);
        } else {
            $_SESSION['sucess'] = 'Cadastrado com sucesso.';
            header("Location: ../admin/lst_admin.php");
        }
    }

    private function edtAdmin() {
        $this->requireAdminSession(Array("admin_control"));
        if (empty($_POST['id_admin']) || !$this->admin->find($_POST['id_admin'])) {
            throw new ExceptionController('Administrador inválido', self::ERROR_CODE_PREFIX . '54');
        }
        $this->admin->name = $this->validateName($_POST['name']);
        $this->admin->email = $this->validateEmail($_POST['email']);
        if ($_POST['new_password'] != '') {
            $this->admin->password = md5(Validate::string($_POST['new_password'], 4));
        }
        $this->admin->cpf = Validate::cpf($_POST['cpf']);
        $this->admin->adress = $_POST['adress'];
        $this->admin->phone = Validate::phone($_POST['phone']);
        $this->admin->cellphone = Validate::phone($_POST['cellphone']);
        $this->admin->user_control = (isset($_POST['user_control']) ? 1 : 0);
        $this->admin->presence_control = (isset($_POST['presence_control']) ? 1 : 0);
        $this->admin->manual_presence_control = (isset($_POST['manual_presence_control']) ? 1 : 0);
        $this->admin->activity_control = (isset($_POST['activity_control']) ? 1 : 0);
        $this->admin->ticket_control = (isset($_POST['ticket_control']) ? 1 : 0);
        $this->admin->admin_control = (isset($_POST['admin_control']) ? 1 : 0);
        $this->admin->config_control = (isset($_POST['config_control']) ? 1 : 0);
        $this->admin->active = 1;

        if (!$this->admin->update(array('id_admin' => $this->admin->id_admin)) > 0) {
            throw new ExceptionController('Não foi possível atualizar o administrador', self::ERROR_CODE_PREFIX . ExceptionController::FAIL_REGISTER);
        } else {
            $_SESSION['sucess'] = 'Administrador atualizado.';
            header("Location: ../admin/lst_admin.php");
        }
    }

    protected static function setSession($email, $password) {
        /**
         * Método que cria sessão de email e senha do usuário corrente, caso ele seja validado
         *
         * @param String $email Email do usuário corrente
         * @param String $password Senha do usuário corrente criptografada
         *
         * */
        $_SESSION['cpf'] = NULL; //setando email
        $_SESSION['email'] = $email; //setando email
        $_SESSION['password'] = $password; //setando senha
        $_SESSION['type_user'] = 1;
    }

    private function login($email, $password) {
        /**
         * Método que valida o login do usuário recebendo por parâmetro email e senha
         *
         * @param String $email Email do usuário corrente
         * @param String $password Senha do usuário corrente criptografada
         *
         * */
        if ($this->admin->validateAdmin($email, $password)) { //realiza validação do usuário buscando dados no banco
            self::setSession($email, $password); //chama método estático setSession
            if (!empty($_SESSION['url'])) { //verifica se existe sessão de url
                $url = $_SESSION['url']; //caso tenha seta a variável url
                if ($url != NULL || $url != '') { //verifica se a variável é nula ou string vazia
                    header("Location: $url"); //redireciona para a url
                    $_SERVER['url'] = NULL; //reseta a sessão da url
                }
            } else {
                $this->admin->retriveAdmin($email); //obtém o objeto do usuário
                header("Location: " . self::URL_CONTROL_PANEL); //redireciona para o painel de controle
                ob_end_flush();
                exit();
            }
        } else {
            throw new ExceptionController('Administrador Inválido.', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_IDENTIFIER);
        }
    }

    public static function setLogin($email, $password) {
        /**
         * Método estático que cria sessão de email e senha do usuário corrente, caso ele seja validado
         *
         * @param Object $admin Objeto do usuário corrente
         *
         * */
        $admin = new Admin();
        if ($admin->validateAdmin($email, $password)) {
            $admin = $admin->retriveAdmin($email);
            $admin->find($admin->idadmin);
            self::setSession($admin->email, $admin->password); //chama método estático setSession

            if (!empty($_SESSION['url'])) { //verifica se existe sessão de url
                $url = $_SESSION['url']; //caso tenha seta a variável url
                if ($url != NULL || $url != '') { //verifica se a variável é nula ou string vazia
                    header("Location: $url"); //redireciona para a url
                    $_SERVER['url'] = NULL; //reseta a sessão da url
                }
            } else {
                header("Location: ../admin/control_panel.php"); //redireciona para o painel de controle
                ob_end_flush();
                exit();
            }
        } else {
            throw new ExceptionController('Administrador Inválido.', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_IDENTIFIER);
        }
    }

    private function sendRequestEmail($email, $subject, $msg) {
        $object_email = new Email();
        $object_email->sendRequestEmail($email, $subject, $msg);
    }

    public function accessRescueRequest() {
        /**
         * Método para enviar email de requisição de mudança de senha
         *
         * @todo Modificar a url enviada para o email do usuário
         * */
        $object_admin = $this->admin->retriveAdmin($_POST['email']);
        if ($object_admin != NULL) {
            if ($_POST['email'] == $object_admin->email) {
                $send_email = new Email();

                $hash = substr(uniqid(), 0, 8);
                $this->changePassword($object_admin->id_admin, $hash);
                $send_email->to = $_POST['email'];
                $send_email->subject = 'Recuperação de Senha';
                $send_email->html = "Sua senha foi modificada:<br/><strong>Nova senha:</strong>$hash.";

                $this->sendRequestEmail($send_email->to, $send_email->subject, $send_email->html);
            } else {
                throw new ExceptionController('Email não está registrado.', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_EMAIL);
            }
        } else {
            throw new ExceptionController('Usuário não encontrado.', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_IDENTIFIER);
        }
    }

    public function changePassword($idadmin, $new_password) {
        if ($this->admin->updatePassword(md5($new_password), $idadmin)) {
            $_SESSION['sucess'] = "Senha modificada com sucesso.";
            $admin = $this->admin->retrive(Array("id_admin" => $idadmin));
            self::setSession($admin->email, md5($_POST['new_password']));
            header("Location: ../admin/control_panel.php");
        } else {
            throw new ExceptionController('Falha ao atualizar.', self::ERROR_CODE_PREFIX . ExceptionController::FAIL_UPDATE);
        }
    }
    
    private function logout() {
        /**
         * Método logout para deslogar o usuário
         *
         * */
        session_start(); //inicia session
        $_SESSION = array(); //atribue um array vazio para a sessão
        session_destroy(); // destrói a sessão
        header("Location: ../admin/login.php"); //redireciona para o login
        ob_end_flush();
    }

    private function setConfigValue() {
        /**
         * Método logout para deslogar o usuário
         *
         * */
        if (empty($_POST['config'])) {
            throw new ExceptionController('selecione uma configuração');
        }
        if (empty($_POST['value'])) {
            throw new ExceptionController('Valor inválido para configuração');
        }
        $this->admin->updateConfigValue($_POST['config'], $_POST['value']);
    }

    private function validateEmail($email) {
        $validate = new Validate();
        if ($validate->validateEmail($email)) {
            if (!$this->admin->searchValue('email', $email, $this->admin->id_admin)) {
                return $email;
            } else {
                throw new ExceptionController('Email já cadastrado', 1101);
            }
        } else {
            throw new ExceptionController('Email inválido', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_EMAIL);
        }
    }

    private function validateName($name) {
        $validate = new Validate();
        if (stripos($name, " ")) {
            if ($validate->validateField($name, 100, 2) && $validate->getWordsNumber($name > 1)) {
                return $name;
            } else {
                throw new ExceptionController('Nome Inválido', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_USER_NAME);
            }
        } else {
            throw new ExceptionController('Informe seu nome e sobrenome', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_USER_NAME);
        }
    }

}

new AdminController();
