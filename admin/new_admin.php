<!DOCTYPE html>
<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
Helper::requireAdminSession($current_user);
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?= Config::TITLE ?></title>
    </head>

    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main>
            <div class="row">
                <section class="col s0 l3">
                    <?php include '../view/partial/adminmenu.php'; ?>
                </section>
                <section class="col s12 l9">
                    <!-- Teal page content  -->
                    <?php Helper::messageBoxRow(); ?>
                    <div class="row">
                        <div class="card">
                            <div class="card-content">
                                <form class="col s12" id="form-newadmin" action="../controller/AdminController.php" method="POST" id="form-admin">
                                    <h5 class="header center blue-text">Novo Administrador</h5>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input placeholder="Nome (obrigatório)" id="name" type="text" name="name" class="validate">
                                            <label for="name">Nome</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 l8">
                                            <input placeholder="Informe um e-mail válido (obrigatório)" id="email" type="email" name="email" class="validate">
                                            <label for="email">Email</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 l6">
                                            <input placeholder="Informe uma senha (obrigatório)" id="password" type="password" name="password" class="validate">
                                            <label for="password">Senha</label>
                                        </div>
                                        <div class="input-field col s12 l6">
                                            <input placeholder="Repita a senha (obrigatório)" id="re-password" type="password" name="re_password" class="validate">
                                            <label for="re-password">Repita a Senha digitada</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m6 l4">
                                            <input type="checkbox" class="filled-in" id="user_control" name="user_control"/>
                                            <label for="user_control">Controle de Usuários</label>
                                        </div>
                                        <div class="input-field col s12 m6 l4">
                                            <input type="checkbox" class="filled-in" id="admin_control" name="admin_control" />
                                            <label for="admin_control">Controle de Administradores</label>
                                        </div>
                                        <div class="input-field col s12 m6 l4">
                                            <input type="checkbox" class="filled-in" id="words_control" name="words_control"/>
                                            <label for="words_control">Controle de Palavras</label>
                                        </div>
                                        <div class="input-field col s12 m6 l4">
                                            <input type="checkbox" class="filled-in" id="gramatical_control" name="gramatical_control"/>
                                            <label for="gramatical_control">Controle das Classes Gramaticais</label>
                                        </div>
                                        <div class="input-field col s12 m6 l4">
                                            <input type="checkbox" class="filled-in" id="translation_control" name="translation_control" />
                                            <label for="translation_control">Controle de Traduções</label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button type="submit" name="option" value="new_admin" class="waves-effect waves-light btn">Enviar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </main>
        <?php include '../view/partial/scripts.php'; ?>
        <script src="../view/js/admin-forms-validation.js"></script>
        <script>
            $("#cpf").mask("999.999.999-99");
            $("#phone").mask("(99)9999-9999?9");
            $("#cellphone").mask("(99)9999-9999?9");
        </script>
    </body>
</html>
