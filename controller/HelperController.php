<?php

include_once '../model/City.php';
include_once '../model/State.php';

class HelperController {

    public function __construct() {

        ob_start();
        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
        }

        switch ($_GET) {
            case isset($_GET['idsheet']):
                $this->SelectTotalValueAcquittance($_GET['idsheet']);
                break;
            case isset($_GET['idstate']):
                $this->selectCity($_GET['idstate']);
                break;
        }
    }

    public function selectCity($idstate) {

        $city = new City();
        $data = $city->read(array('id_state' => $idstate));
        foreach ($data as $row) {
            $arrCities[$row['id_city']] = $row['name'];
        }
        echo "<label class='left-align col s12'>Cidade</label>"
        . "<div class='input-field col s12'>"
        . "<select name = 'id_city' id = 'combobox_city' class='browser-default'>"
        . "<option value='' disabled selected>Selecione...</option>";
        foreach ($arrCities as $value => $nome) {
            echo "<option value='{$value}'>{$nome}</option>";
        }
        echo '</select>';
        ob_end_flush();
    }
}

new HelperController;
