<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
include_once '../model/Language.php';
include_once '../model/GramaticalClass.php';
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?php echo Config::TITLE ?></title>
        <link href="../view/css/jquery.steps.css" type="text/css" rel="stylesheet" />
        <link href="../view/css/main.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main>
            <div class="container">
                <div class="section">
                    <?php
                    Helper::messageBoxRow();
                    ?>
                    <div class="card">
                        <div class="card-content">
                            <?php include './partial/form_new_word.php'; ?>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php include '../view/partial/scripts.php'; ?>
        <script>
            $("#form-new-word").validate({
                rules: {
                    language: {
                        required: true
                    },
                    word: {
                        required: true
                    },
                    id_gramatical_class: {
                        required: true
                    },
                    phrase_example_first_language: {
                        required: true
                    }
                },
                messages: {
                    language: {
                        required: "Selecione um idioma."
                    },
                    word: {
                        required: "Digite uma palavra."
                    },
                    id_gramatical_class: {
                        required: "Selecione uma Classe Gramatical."
                    },
                    phrase_example_first_language: {
                        required: "Digite uma frase de exemplo com a palavra utilizada."
                    }
                }
            });
        </script>
        <script>
            
        </script>
        <script src="../view/js/jquery.steps.js"></script>
        <script>
            $("#example-basic").steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                autoFocus: true,
                onStepChanging: function (event, currentIndex, newIndex) {
                    form = $('#form-new-word');
                    form.validate().settings.ignore = ":disabled,:hidden";
                    return form.valid();
                },
                onFinishing: function (event, currentIndex) {
                    form.validate().settings.ignore = ":disabled";
                    return form.valid();
                },
                onFinished: function (event, currentIndex) {
                    $('#form-new-word').submit();
                }
            });
        </script>
    </body>
</html>