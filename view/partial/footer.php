<footer class="page-footer grey lighten-2">
    <div class="container">
        <div class="row">
            <div class="col s6 left-align">
                <h5 class="teal-text text-darken-4">Menu</h5>
                <ul>
                    <li><a class="grey-text text-darken-2" href="./dictionary.php"><i class="rules-icons material-icons small">book</i> O DICIONÁRIO</a></li>
                    <li><a class="grey-text text-darken-2" href="./index.php"><i class="rules-icons material-icons small">zoom_in</i> PESQUISAR</a></li>
                    <li><a class="grey-text text-darken-2" href="./contact.php"><i class="rules-icons material-icons small">mail_outline</i> CONTATO</a></li>
                </ul>
            </div>
            <div class="col s6 right-align">
                <h5 class="teal-text text-darken-4">Sobre o Dicionário</h5>
                <p class="grey-text text-darken-2">Este é um projeto idealizado pelos Professores <strong>Marcio Alexandre de Oliveira Reis</strong> e <strong>Cristina Spohr Reis</strong>
                    com o intuito de facilitar a busca de termos técnicos de matemática em outros idiomas.</p>
            </div>
        </div>
    </div>
    <div class="footer-copyright teal darken-4">
        <div class="container grey-text text-lighten-3">
            © 2016 Copyright Text
            <div class="right">Desenvolvedores: <b><a target="_blank" class="white-text" href="https://www.facebook.com/feliperodalves?fref=ts">Felipe Alves</a></b> e <b><a target="_blank" class="white-text" href="https://www.facebook.com/jandersonmix">Janderson Bezerra</a></b></div>
        </div>
    </div>
</footer>
