(function ($) {
    $(document).ready(function () {
        $('.tooltipped').tooltip({delay: 50});
        $('.tooltipped').tooltip('remove');
        $('.collapsible').collapsible({
            accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
        });
        $('.button-collapse').sideNav();
    });
})(jQuery); // end of jQuery name space