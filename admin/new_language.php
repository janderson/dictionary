<!DOCTYPE html>
<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
Helper::requireAdminSession($current_user);
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?= Config::TITLE ?></title>
    </head>

    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main>
            <div class="row">
                <section class="col s0 l3">
                    <?php include '../view/partial/adminmenu.php'; ?>
                </section>
                <section class="col s12 l9">
                    <!-- Teal page content  -->
                    <?php Helper::messageBoxRow(); ?>
                    <div class="row">
                        <div class="card">
                            <div class="card-content">
                                <form class="col s12" id="form-newadmin" action="../controller/LanguageController.php" method="POST" id="form-admin">
                                    <h5 class="header center blue-text">Novo Idioma</h5>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input placeholder="Idioma (obrigatório)" id="language" type="text" name="name" class="validate">
                                            <label for="language">Idioma</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 l8">
                                            <input placeholder="País de Origem (obrigatório)" id="home_country" type="text" name="home_country" class="validate">
                                            <label for="home_country">País de Origem</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <textarea placeholder="Descrição completa" id="description" type="text" name="description" maxlength="3000" class="validate materialize-textarea"></textarea>
                                            <label for="description">Descrição</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button type="submit" name="option" value="new_language" class="waves-effect waves-light btn">Enviar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </main>
        <?php include '../view/partial/scripts.php'; ?>
        <script src="../view/partial/js/admin-forms-validation.js"></script>
        <script>
            $("#cpf").mask("999.999.999-99");
            $("#phone").mask("(99)9999-9999?9");
            $("#cellphone").mask("(99)9999-9999?9");
        </script>
    </body>
</html>
