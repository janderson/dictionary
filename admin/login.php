<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?= Config::TITLE ?></title>
    </head>
    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main>
            <div class="row">
                <?php Helper::messageBoxRow(); ?>
            </div>
            <div class="row">
                <div class="col s12 m6 l3">
                    <div class="row">
                        <form name="login" id="form-login" class="card white" action="../controller/AdminController.php" method="POST">
                            <div class="card-content">
                                <h5 class="header center green-text">Acesso Administrativo</h5>
                                <div class="input-field col s12">
                                    <input placeholder="SEU E-MAIL (obrigatório)" id="email" type="email" name="login" class="validate">
                                    <label for="login">E-mail</label>
                                </div>
                                <div class="input-field col s12">
                                    <input placeholder="SUA SENHA (obrigatório)" id="password" type="password" name="password" class="validate" maxlength="30">
                                    <label for="password">Senha</label>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <button type="submit"  name="option" value="login" class="waves-effect waves-light btn">LOGIN</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col s12 m6 l9">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-title green-text text-darken-3 center-align">FUTURO LOGO DO <?= Config::TITLE ?></div>
                            <div class="col s12 l8 offset-l2">
                                <img class="responsive-img section" src="../view/images/logo_utfpr_pb.png" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php include '../view/partial/scripts.php'; ?>
        <script src="../view/js/admin-forms-validation.js"></script>
        <script>
            $("#form-event").validate({
                rules: {
                    email: {
                        required: true
                    },
                    password: {
                        required: true
                    }
                },
                messages: {
                    email: {
                        required: "Insira um email."
                    },
                    password: {
                        required: "Insira uma senha.",
                        minlenght: "Tanho mínimo: 6 caracteres"
                    }
                }
            });
        </script>
    </body>
</html>