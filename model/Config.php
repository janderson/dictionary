<?php

class Config {

    const TITLE = "DicionárioMulti";
   
    static public function getDescriptionKey($key) {
        try {
            switch ($key) {
                //ALL
                case 'name': return 'Nome';
                case 'id_user': return 'Usuário';
                //WORD
                case 'id_word': return 'ID';
                case 'word': return 'Palavra';
                case 'plural': return 'Plural';
                //TRANSLATION
                    
                //LANGUAGE
                
                default : return $key;
            }
        } catch (Exception $exc) {
            return NULL;
        }
    }
}
