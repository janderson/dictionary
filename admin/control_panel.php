<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
Helper::requireAdminSession($current_user);
require_once "../model/Word.php";
$word = new Word();
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?= Config::TITLE ?></title>
    </head>
    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main>
            <div class="row">
                <section class="col s0 l3">
                    <?php include '../view/partial/adminmenu.php'; ?>
                </section>
                <section class="col s12 l9">
                    <?php Helper::messageBoxRow(); ?>
                    <div class="row">
                        <div class="col s12 l6">
                            <div class="card">
                                <div class="card-content green darken-1">
                                    <div class="card-title">AÇÕES PRINCIPAIS</div>
                                </div>
                                <div class="col s12 l6">
                                    <a href="./new_word.php">
                                        <div class="card">
                                            <div class="card-content center-align">
                                                <div class="card-title grey-text text-darken-3 button-margin">Cadastrar Nova Palavra</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col s12 l6">
                                    <a href="./new_translate.php">
                                        <div class="card">
                                            <div class="card-content center-align">
                                                <div class="card-title grey-text text-darken-3 button-margin">Palavras sem Tradução </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 l6">
                            <div class="card">
                                <div class="card-content green darken-1">
                                    <div class="card-title">INFORMAÇÕES</div>
                                </div>
                                <div class="card-content hoverable">
                                    <ul class="collection with-header">
                                        <li class="collection-item"><div>Usuários cadastrados <span class="badge circle red accent-2 grey-text text-darken-4"><?php echo count($current_user->read()); ?></span></div></li>
                                        <li class="collection-item"><div>Usuários cadastrados hoje<span class="badge circle yellow accent-2 grey-text text-darken-4"><?php echo $current_user->getUserRegistedToday() ?></span></div></li>
                                        <li class="collection-item"><div>Palavras cadastradas<span class="badge circle green accent-2 grey-text text-darken-4"><?php echo count($word->read()); ?></span></div></li>
                                        <li class="collection-item"><div>Palavras cadastradas hoje<span class="badge circle purple accent-2 grey-text text-darken-4"><?php echo $word->getWordRegistedToday() ?></span></div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </main>
        <?php include '../view/partial/scripts.php'; ?>
    </body>
</html>