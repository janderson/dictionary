<?php if ($current_user instanceof Admin) { ?>
    <div class="collection hide-on-med-and-down">
        <a href="../admin/control_panel.php" class="collection-item"><i class="material-icons">chrome_reader_mode</i>Painel de Controle</a>
        <a href="../admin/lst_user.php" class="collection-item"><i class="material-icons">supervisor_account</i>Todos os Usuários</a>
        <a href="../admin/inbox.php" class="collection-item"><i class="material-icons">mail_outline</i>Caixa de Entrada</a>
        <a href="../admin/new_word.php" class="collection-item"><i class="material-icons">translate</i>Fazer Tradução</a>
        <a href="../admin/lst_words.php" class="collection-item"><i class="material-icons">spellcheck</i>Palavras</a>
        <a href="../admin/lst_word_untranslated.php" class="collection-item"><i class="material-icons">spellcheck</i>Palavras sem Tradução</a>
        <a href="../admin/new_admin.php" class="collection-item"><i class="material-icons">person_add</i>Cadastrar Administrador</a>
        <a href="../admin/lst_admin.php" class="collection-item"><i class="material-icons">group</i>Listar Administrador</a>
        <a href="../admin/new_language.php" class="collection-item"><i class="material-icons">view_module</i>Cadastrar Idioma</a>
        <a href="../admin/lst_language.php" class="collection-item"><i class="material-icons">assignment_ind</i>Listar Idioma</a>
        <a href="../admin/new_gramatical_class.php" class="collection-item"><i class="material-icons">local_library</i>Cadastrar Classe Gramatical</a>
        <a href="../admin/lst_gramatical_class.php" class="collection-item"><i class="material-icons">local_library</i>Classes Gramaticais</a>
    </div>
    <?php
}?>