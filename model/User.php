<?php

require_once '../model/Crud.php';
require_once '../model/City.php';

class User extends Crud {
    /* Model de identidade, representa a tabela das entidades no banco */

    const ALLOW_GENERATE_CERTIFICATE = true;

    protected $id_user;
    protected $name;
    protected $email;
    protected $password;
    protected $id_city;
    protected $_city;
    protected $_dtt_register;
    private $_table = "USER"; //Nome da tabela
    private $_list = 'lst_user'; //nome da view
    private $_key_field = 'id_user'; //chave da tabela

    //Método SET

    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    //Método GET
    function __get($atribute) {
        return $this->$atribute;
    }

    //Método CONSTRUCT
    function __construct($iduser = NULL) {
        parent::__construct();
        if ($iduser > 0) {
            $this->find($iduser);
        }
    }

    public function getSerialize($id = NULL) {
        $this->retriveAll(($id != NULL ? Array("id_user" => $id) : ''));
        $s = serialize($this);
        return $s;
    }

    public function __sleep() {
        return array('id_user', 'username', 'email', 'name', 'id_city',);
    }

    public function __wakeup() {
        return $this;
    }

    public function find($id) {
        if (parent::find(Array($this->_key_field => $id))) {
            $this->_city = new City($this->id_city);

            return TRUE;
        } else {
            return NULL;
        }
    }

    public function getCity() {
        $city = new City();
        $city->find(array("id_city" => $this->id_city));

        return $city;
    }

    public function validateUser($email, $password) {
        /*
         * Método para validação de usuário apartir de email e senha
         * 
         * @param String $email email para realizar busca
         * @param String $password senha para realizar busca
         * @return BOOL TRUE caso encontrado usuário registrado com email e senha passados, FALSE caso contrário
         */

        $data = $this->read(array("email" => $email, 'password' => $password));

        if (count($data) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function updatePassword($password, $iduser) {

        $stmt = $this->_db->prepare("UPDATE `$this->_table` SET password='$password' WHERE id_user = $iduser ;");

        $stmt->execute();

        $count = $stmt->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            $erros = $stmt->errorInfo();
            Logger::log_array(__CLASS__, $erros);
            throw new Exception('Sua senha não foi modificada.', $erros[1]);
        }
    }

    public function retriveUser($email) {

        /**
         * (PHP 5 &gt;= 5.1.0, PECL pdo &gt;= 0.1.0)<br/>
         * Busca o registro no banco de dados e retorna um User com estado
         * @param array $filter filtro para realizar a busca
         * @return object  <b>User</b>
         */
        $data = $this->retrive(array("email" => $email));

        return $data;
    }

    public function getDescriptionAtribute($id) {

        $this->find($id);
        $atributes = get_class_vars(get_class($this));
        foreach ($atributes as $key => $value) {
            $atributes[$key] = $this->$key;
        }

        return $atributes;
    }

    public function getList() {
        try {
            $stmt = $this->_db->prepare("SELECT `id_user` AS `_id`, `id_user`, `name`, `email` FROM `USER` ORDER BY `name` DESC");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }

    static public function getDescriptionKey($key) {
        try {
            switch ($key) {
                case 'id_user':return 'ID';
                case 'name':return 'Nome';
                case 'email':return 'Email';
            }
        } catch (Exception $exc) {
            return NULL;
        }
    }

    public function validateCredentials($array) {
        return FALSE;
    }

}

?>
