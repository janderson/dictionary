<ul id="user-dropdown" class="dropdown-content top-menu green darken-4 ">
    <li class="divider"></li>
    <?php
    if ($current_user instanceof Admin) {
        echo '<li><a href="../admin/control_panel.php">Painel de controle</a></li>';
    } elseif ($current_user instanceof User) {
        echo '<li><a href="../view/profile.php">Seu perfil</a></li>'
        . '<li><a href="../view/control_panel.php">PAINEL DE CONTROLE</a></li>';
    }
    ?>
    <li class="divider"></li>
    <li><a href="../controller/UserController.php?option=logout">Sair</a></li>
</ul>
<nav class="top-nav green darken-4 top-menu" role="navigation">
    <div class="nav-wrapper container">
        <a id="logo-container" href="../view/index.php">
            <img src="./images/logo.png" class="responsive-img" />
        </a>
        <?php
        if (!empty($current_user)) {
            ?>
            <ul class="right hide-on-med-and-down col l4">
                <li>
                    <a class="dropdown-button" href="#options" data-activates="user-dropdown">
                        <?php echo 'Olá ' . Helper::formatNameToShort($current_user->name); ?><i class="material-icons right">arrow_drop_down</i>
                    </a>
                </li>
            </ul>
            <?php
        } else {
            ?>
            <ul class="right hide-on-med-and-down l2">
                <li class=""><a href="./index.php">Login</a></li>
            </ul>
            <?php
        }
        ?>
        <ul class="right hide-on-med-and-down col l6" id="menu-links">
            <li><a href="../view/dictionary.php">O DICIONÁRIO</a></li>
            <li><a href="../view/index.php">PESQUISAR</a></li>
            <li><a href="../view/contact.php">CONTATO</a></li>
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
</nav>