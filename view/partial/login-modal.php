<div class="row">
    <div id="modal-login" class="modal modal-fixed-footer col 12 l4 offset-l4">
        <div class="modal-content modal-login">
            <h5 class="center-align">Para cadastrar palavras é necessário estar logado.</h5>
            <form action="../controller/UserController.php" method="POST" name="login" id="form-login">
                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" name="login" class="validate">
                        <label for="login">Login</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input type="password" name="password" class="validate">
                        <label for="password">Senha</label>
                    </div>
                    <div class="col s12">
                        <a href="./request_password.php">Esqueceu sua senha?</a>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 center-align">
                        <input type='hidden' name="redirect" value="<?= $_SERVER['PHP_SELF']; ?>" />
                        <button type="submit"  name="option" value="login" class="waves-effect waves-light btn button-margin">LOGIN</button>

                        <a  type="button" class="facebook waves-effect waves-light btn teal darken-3 button-margin" name="entrar" href="./register.php" >
                            Cadastre-se
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>