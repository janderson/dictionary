<?php

require_once("Crud.php");

class Language extends Crud {

    protected $id_language;
    protected $name; 
    protected $home_country;
    protected $description;
    protected $_table = 'LANGUAGE';
    protected $_list = 'lst_language';
    protected $_key_field = 'id_language';

    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    function __get($atribute) {
        return $this->$atribute;
    }

    function __construct($idlanguage = NULL) {
        parent::__construct();
        if ($idlanguage > 0) {
            $this->find($idlanguage);
        }
    }

    public function find($id) {
        if (parent::find(Array($this->_key_field => $id))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getSerialized($id = NULL) {
        $this->retriveAll(($id != NULL ? Array($this->_key_field => $id) : ''));
        $serialized = serialize($this);
        return $serialized;
    }

    public function __sleep() {
        return array('id_language');
    }

    public function __wakeup() {
        return $this;
    }
    
    public function getList($rows = 50) {
        try {
            $stmt = $this->_db->prepare("SELECT * FROM get_languages");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }
    
    static public function getDescriptionKey($key) {
        try {
            switch ($key) {
                case 'id_gramatical_class':return 'ID';
                case '_id':return 'ID';
                case 'name':return 'Classe Gramatical';
                case 'home_country':return 'País de Origem';
                case 'description':return 'Descrição';
                default : return $key;
            }
        } catch (Exception $exc) {
            return NULL;
        }
    }
}
