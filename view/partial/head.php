<meta charset="utf-8">
<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="../view/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
<link href="../view/css/jquery.datetimepicker.css" type="text/css" rel="stylesheet" />
<link href="../view/css/style.css" type="text/css" rel="stylesheet" />

