CREATE USER 'dictionary_utfpr'@'localhost' IDENTIFIED BY 'utf_pb_dic';

GRANT SELECT ON TABLE `dicionariomulti`.* TO 'dictionary_utfpr'@'localhost' ;
GRANT SELECT, INSERT, TRIGGER ON TABLE `dicionariomulti`.* TO 'dictionary_utfpr'@'localhost' ;
GRANT SELECT, INSERT, TRIGGER, UPDATE, DELETE ON TABLE `dicionariomulti`.* TO 'dictionary_utfpr'@'localhost' ;
GRANT EXECUTE ON  `dicionariomulti`.* TO 'dictionary_utfpr'@'localhost' ;