<?php

require_once '../model/User.php';
require_once '../model/Admin.php';
require_once '../model/Logger.php';
require_once '../model/Config.php';
require_once './Validate.php';
require_once './ExceptionController.php';

class Controller {

    const ERROR_CODE_PREFIX = '70';
    const SESSION_ERROR_CODE_PREFIX = '10';
    const URL_LOGIN_PAGE = '../view/login.php';
    const JSON_RETURN = 'JSON';
    const HTTP_RETURN = NULL;

    protected $_currentUser;
    protected $_currentAdmin;

    public function __construct() {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
        }
        $this->_currentUser = new User();
        $this->_currentAdmin = new Admin();
        
        $GLOBALS['config'] = new Config();
    }

    protected function findSession() {
        if (isset($_SESSION['email']) && isset($_SESSION['password'])) {
            $email = $_SESSION['email'];
            $password = $_SESSION['password'];
            if ($this->_currentUser->validateUser($email, $password) && $this->_currentUser->active = 1) {
                $this->_currentUser = $this->_currentUser->retriveUser($email);
            } else {
                $this->_currentAdmin = $this->_currentAdmin->retriveAdmin($email);
            }
        } else {
            throw new Exception('Por favor realize o login para continuar.', self::SESSION_ERROR_CODE_PREFIX . '10');
        }
        return TRUE;
    }
    protected function requireUserSession() {
        if (isset($_SESSION['email']) && isset($_SESSION['password'])) {
            $email = $_SESSION['email'];
            $password = $_SESSION['password'];
            if ($this->_currentUser->validateUser($email, $password) && $this->_currentUser->active = 1) {
                $this->_currentUser = $this->_currentUser->retriveUser($email);
            } else {
                session_destroy();
                throw new Exception('Sessão inválida, Por favor realize o login para continuar.', self::SESSION_ERROR_CODE_PREFIX . '10');
            }
        } else {
            throw new Exception('Por favor realize o login para continuar.', self::SESSION_ERROR_CODE_PREFIX . '10');
        }
        return TRUE;
    }

    protected function requireAdminSession($credentials = NULL) {
        if (isset($_SESSION['email']) && isset($_SESSION['password'])) {
            $email = $_SESSION['email'];
            $password = $_SESSION['password'];
            if ($this->_currentAdmin->validateAdmin($email, $password) && $this->_currentAdmin->active = 1) {
                $this->_currentAdmin = $this->_currentAdmin->retriveAdmin($email);
            } else {
                session_destroy();
                throw new Exception('Sessão inválida, Por favor realize o login para continuar.', self::SESSION_ERROR_CODE_PREFIX . '10');
            }
        } else {
            throw new Exception('Por favor realize o login para continuar.', self::SESSION_ERROR_CODE_PREFIX . '20');
        }
        if (empty($credentials)) {
            return TRUE;
        } else {
            return $this->requireAdminCredentials($credentials);
        }
    }

    protected function requireAdminCredentials($credentials) {
        $return = FALSE;
        if (count($credentials) > 0) {
            foreach ($credentials as $key) {
                if ($this->_currentAdmin->$key == 1) {
                    $return = TRUE;
                } else {
                    throw new ExceptionController("Sem credenciais para realizar essa modificação.", Controller::ERROR_CODE_PREFIX . '00');
                }
            }
        } else {
            $return = TRUE;
        }
        return $return;
    }

    // ===============================================
    // ===============================================

    public static function redirect($url) {
        header('location: ' . $url);
    }

    // ===============================================
    // ===============================================

    private function validateEmail($email) {
        $validate = new Validate();
        if ($validate->validateEmail($email)) {
            if (!$this->user->searchValue('email', $email, $this->user->id_user)) {
                return $email;
            } else {
                throw new ExceptionController('Email já cadastrado', 1101);
            }
        } else {
            throw new ExceptionController('Email inválido', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_EMAIL);
        }
    }

    private function validateName($name) {
        $validate = new Validate();
        if (stripos($name, " ")) {
            if ($validate->validateField($name, 100, 2) && $validate->getWordsNumber($name > 1)) {
                return $name;
            } else {
                throw new ExceptionController('Nome Inválido', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_USER_NAME);
            }
        } else {
            throw new ExceptionController('Insira seu sobrenome', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_USER_NAME);
        }
    }

    private function validateCpf($cpf) {
        $validate = new Validate();
        $cpf = str_replace(array('.', '-'), '', $cpf);
        if ($validate->validateCpf($cpf)) {
            if (!$this->user->searchValue('cpf', $cpf, $this->user->id_user)) {
                return $cpf;
            } else {
                throw new ExceptionController('CPF já cadastrado', 1101);
            }
        } else {
            throw new ExceptionController('CPF inválido', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_EMAIL);
        }
    }

    private function formatDate($date) {
        $date = str_replace('/', '-', $date);
        return date('Y-m-d', strtotime($date));
    }

}
