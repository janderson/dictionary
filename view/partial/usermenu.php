<ul id="user-menu" class="card grey lighten-5" >
    <?php
    if ($current_user instanceof User) {
        include './partial/usermenuitens.php';
    } else {
        include './partial/adminmenu.php';
    }
    ?>
</ul>