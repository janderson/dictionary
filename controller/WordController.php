<?php

require_once './Controller.php';
require_once './ExceptionController.php';
require_once '../model/Word.php';
require_once '../model/Translation.php';
require_once '../model/Language.php';
require_once '../model/LanguageWord.php';
require_once '../model/GramaticalClass.php';
require_once '../model/Category.php';
require_once '../model/WordGramaticalClass.php';

class WordController extends Controller {

    const ERROR_CODE_PREFIX = '10';
    const CHANGE_PASSWORD = "<a href='http://localhost/frade/admin/req_new_password.php";
    const URL_CONTROL_PANEL = "../admin/control_panel.php";

    private $word;
    private $language;
    private $language_word;
    private $translation;
    private $gramatical_class;
    private $category;
    private $word_gramatical_class;

    public function __construct() {
        parent::__construct();
        try {
            $this->word = new Word();
            $this->language = new Language();
            $this->language_word = new LanguageWord();
            $this->gramatical_class = new GramaticalClass();
            $this->word_gramatical_class = new WordGramaticalClass();
            $this->translation = new Translation();
            $this->category = new Category();
            switch ($_SERVER['REQUEST_METHOD']) {
                case "POST":
                    switch ($_POST['option']) {
                        case 'new_translate':
                            $this->newTranslate();
                            break;
                        case 'edt_word':
                            $this->edtWord();
                            break;
                        case 'research_word':
                            $this->researchWord($_POST['word_research'], $_POST['id_language']);
                            break;
                        default:
                            break;
                    }
                    break;
                case "GET":
                    switch ($_GET['option']) {
                        case 'filter_word':
                            $this->filterWord($_GET['letter']);
                            break;
                        default:
                            break;
                    }
                    break;
                default :
                    throw new ExceptionController('Método não suportado!', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_OPTION, NULL);
            }
        } catch (Exception $e) {
            if ($e instanceof ExceptionController || $e instanceof CrudException) {
                $_SESSION['error_code'] = $e->getCode();
                $_SESSION['error_message'] = $e->getMessage();
                if (empty($_SERVER['HTTP_REFERER'])) {
                    header("Location: " . ExceptionController::PATH_ERROR_PAGE);
                } else {
                    header("Location: {$_SERVER['HTTP_REFERER']}");
                }
            } else {
                ExceptionController::handleException($e);
            }
        }
    }

    private function filterWord($filter) {
        header("Location: ../view/dictionary.php?filter=" . $filter);
    }

    private function researchWord($word, $language) {
        $id_word = $this->word->read(Array("word" => $word))[0]['id_word'];
        $data = $this->language_word->read(Array("id_word" => $id_word));
        if (count($data) > 0) {
            header("Location: ../view/result_of_research.php?language_word=" . $data[0]['id_language_word'] . (!empty($language) ? "&id_language=" . $language : ''));
        } else {
            header("Location: ../view/result_of_research.php");
        }
    }

    private function newTranslate() {
        try {
            $this->findSession();
            for ($i = 1; $i <= 4; $i++) {
                $data[] = [
                    "word" => $_POST['word'][$i - 1],
                    "language" => $_POST['language'][$i - 1],
                    "plural" => $_POST['plural'][$i - 1],
                    "category" => $_POST['category'][$i - 1],
                    "gramatical_class" => $_POST['gramatical_class'][$i - 1],
                    "phase_ex" => $_POST['phrase_example'][$i - 1]
                ];
            }
            foreach ($data AS $row => $fields) {
                $id_languages_words[] = $this->validateRegisters($fields);
            }
            $res = $this->registerTranslaters($id_languages_words);
            if ($res) {
                $_SESSION['sucess'] = 'Informações Registradas com Sucesso.';
                if (isset($_POST['redirect'])) {
                    header("location: " . $_POST['redirect'] . "?language_word=" . $$id_languages_words[0] . "&id_language=" . $data[0]['language']);
                }
            } else {
                throw new ExceptionController('Falha no Registro', self::ERROR_CODE_PREFIX . ExceptionController::FAIL_REGISTER);
            }
            exit();
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    private function validateLanguage($data) {
        foreach ($data AS $row => $fields) {
            if ($row == 0) {
                $language = $data[$row + 1]['language'];
            } elseif ($row != 3) {
                $language = $data[$row]['language'];
            }
            if ($fields['language'] == $language) {
                $return_data = array(
                    'message' => 'Erro: Idiomas iguais;',
                    'code' => 1
                );
            }
        }
    }

    private function registerTranslaters($languages_words) {
        if (!empty($languages_words[0]) && !empty($languages_words[1])) {
            $this->newTranslation($languages_words[0], $languages_words[1]);
            if (!empty($languages_words[2])) {
                $this->newTranslation($languages_words[0], $languages_words[2]);
                $this->newTranslation($languages_words[1], $languages_words[2]);
                if (!empty($languages_words[3])) {
                    $this->newTranslation($languages_words[0], $languages_words[3]);
                    $this->newTranslation($languages_words[1], $languages_words[3]);
                    $this->newTranslation($languages_words[2], $languages_words[3]);
                }
            }
        } elseif (!empty($languages_words[2])) {
            $this->newTranslation($languages_words[0], $languages_words[2]);
            if (!empty($languages_words[3])) {
                $this->newTranslation($languages_words[0], $languages_words[3]);
                $this->newTranslation($languages_words[2], $languages_words[3]);
            }
        } elseif (!empty($languages_words[3])) {
            $this->newTranslation($languages_words[0], $languages_words[3]);
        }
        return TRUE;
    }


    private function validateRegisters($data) {
        $id_language_word;
        if (!empty($data['category'])) {
            $id_category = $this->registerCategory($data['category']);
        }
        if (!empty($data['word'])) {
            $id_word = $this->newWord($data['word'], $data['plural'], (isset($id_category) ? $id_category : ''));
            if (!empty($data['language'])) {
                $id_language_word = $this->newLanguageWord($id_word, $data['language']);
            }
        }
        if (!empty($data['gramatical_class'])) {
            $id_class = $this->newGramaticalClass($data['gramatical_class']);
        }
        if (isset($id_word) && isset($id_class)) {
            $this->newWordGramaticalClass($id_word, $data['phase_ex'], $id_class);
        }
        return $id_language_word;
    }

    private function registerCategory($category) {
        $data = $this->category->read(Array("name" => $category));
        if (count($data) <= 0) {
            $this->category->name = $category;
            $this->category->description = '';
            $id = $this->category->create(array('id_category' => $this->category->id_category));
            if ($id <= 0) {
                throw new ExceptionController('Falha ao Cadastrar', self::ERROR_CODE_PREFIX . ExceptionController::FAIL_REGISTER);
            } else {
                return $id;
            }
        } else {
            return $data[0]['id_category'];
        }
    }

    private function newGramaticalClass($class) {
        if ($this->_currentUser instanceof User || $this->_currentAdmin instanceof Admin) {
            $data = $this->gramatical_class->read(Array("name" => $class));
            if (count($data) <= 0) {
                $this->gramatical_class->name = $class;
                $this->gramatical_class->description = '';
                $id = $this->gramatical_class->create(array('id_gramatical_class' => $this->gramatical_class->id_gramatical_class));
                if ($id <= 0) {
                    throw new ExceptionController('Falha ao Cadastrar', self::ERROR_CODE_PREFIX . ExceptionController::FAIL_REGISTER);
                } else {
                    return $id;
                }
            } else {
                return $data[0]['id_gramatical_class'];
            }
        } else {
            throw new ExceptionController('Você precisa estar logado para cadastrar uma tradução', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_SESSION);
        }
    }

    private function newLanguageWord($word, $language) {
        if ($this->_currentUser instanceof User || $this->_currentAdmin instanceof Admin) {
            $this->language_word->id_word = $word;
            $this->language_word->id_language = $language;
            $id = $this->language_word->create(array('id_language_word' => $this->language_word->id_language_word));
            if ($id <= 0) {
                throw new ExceptionController('Falha ao Cadastrar', self::ERROR_CODE_PREFIX . ExceptionController::FAIL_REGISTER);
            } else {
                return $id;
            }
        } else {
            throw new ExceptionController('Você precisa estar logado para cadastrar uma tradução', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_SESSION);
        }
    }

    private function newWord($word, $plural, $id_category) {
        if ($this->_currentUser instanceof User || $this->_currentAdmin instanceof Admin) {
            $this->word->word = $word;
            $this->word->plural = $plural;
            $this->word->id_category = $id_category;
            $this->word->id_user = ($this->_currentUser->id_user != NULL ? $this->_currentUser->id_user : $this->_currentAdmin->id_admin);
            $id = $this->word->create(array('id_word' => $this->word->id_word));
            if ($id <= 0) {
                throw new ExceptionController('Falha ao Cadastrar', self::ERROR_CODE_PREFIX . ExceptionController::FAIL_REGISTER);
            } else {
                return $id;
            }
        } else {
            throw new ExceptionController('Você precisa estar logado para cadastrar uma tradução', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_SESSION);
        }
    }

    private function edtWord() {
        $this->findSession();
        $id_word = $this->updateWord($_POST['id_word'], $_POST['word'], $_POST['plural']);
        $res = $this->updateTranslation($id_word);
        if ($res) {
            $_SESSION['sucess'] = 'Tudo certo.';
            if ($this->_currentUser->id_user != NULL) {
                header("Location: ../view/new_word.php");
            } else {
                header("Location: ../admin/lst_words.php");
            }
        } else {
            throw new ExceptionController('Falha no Registro', self::ERROR_CODE_PREFIX . ExceptionController::FAIL_REGISTER);
        }
    }

    private function updateWord($id, $word, $plural) {
        if ($this->_currentUser->id_user != NULL || $this->_currentAdmin->id_admin != NULL) {
            $this->word->find($id);
            $this->word->word = $word;
            $this->word->plural = $plural;
            $this->word->id_user = ($this->_currentUser->id_user != NULL ? $this->_currentUser->id_user : $this->_currentAdmin->id_admin);
            $res = $this->word->update(array('id_word' => $this->word->id_word));
            if ($res < 0) {
                throw new ExceptionController('Falha ao Cadastrar', self::ERROR_CODE_PREFIX . ExceptionController::FAIL_REGISTER);
            } else {
                return $id;
            }
        } else {
            throw new ExceptionController('Você precisa estar logado para cadastrar uma tradução', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_SESSION);
        }
    }

    private function newWordGramaticalClass($id_word, $phrase, $id_class) {
        if ($this->_currentUser->id_user != NULL || $this->_currentAdmin->id_admin != NULL) {
            $this->word_gramatical_class->id_word = $id_word;
            $this->word_gramatical_class->id_gramatical_class = $id_class;
            $this->word_gramatical_class->example_sentence = $phrase;
            $this->word_gramatical_class->create(array('id_word' => $this->word->id_word, 'id_gramatical_class' => $this->word_gramatical_class->id_gramatical_class));
            return TRUE;
        } else {
            throw new ExceptionController('Você precisa estar logado para cadastrar uma tradução', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_SESSION);
        }
    }

    private function updateWordGramaticalClass($id_word, $phrase, $id_class) {
        if ($this->_currentUser->id_user != NULL || $this->_currentAdmin->id_admin != NULL) {
            $this->word_gramatical_class->id_gramatical_class = $id_class;
            $this->word_gramatical_class->id_word = $id_word;
            $this->word_gramatical_class->example_sentence = $phrase;
            $this->word_gramatical_class->update(array('id_word' => $this->word->id_word, 'id_gramatical_class' => $this->word_gramatical_class->id_gramatical_class));
            return TRUE;
        } else {
            throw new ExceptionController('Você precisa estar logado para cadastrar uma tradução', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_SESSION);
        }
    }

    private function newTranslation($id_language_word1, $id_language_word2) {
        $this->translation->id_language_word1 = $id_language_word1;
        $this->translation->id_language_word2 = $id_language_word2;
        $id1 = $this->translation->create(array('id_language_word1' => $this->translation->id_language_word1, "id_language_word2" => $this->translation->id_language_word2));
        $this->translation->id_language_word1 = $id_language_word2;
        $this->translation->id_language_word2 = $id_language_word1;
        $id2 = $this->translation->create(array('id_language_word1' => $this->translation->id_language_word1, "id_language_word2" => $this->translation->id_language_word2));
        if ($id2 != 0 || $id1 != 0) {
            throw new ExceptionController('Falha ao Cadastrar', self::ERROR_CODE_PREFIX . ExceptionController::FAIL_REGISTER);
        } else {
            return TRUE;
        }
    }

    private function updateTranslation($id_word, $id_language) {
        $translation = $this->translation->read(array("id_word" => $id_word));
        $this->translation->id_word = $id_word;
        $this->translation->id_language = $id_language;
        $this->translation->id_translation1 = $translation[0]['id_translation1'];
        $id1 = $this->translation->update(array('id_word' => $translation[0]['id_word']));
        return TRUE;
    }

}

new WordController();
