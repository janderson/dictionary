<!DOCTYPE html>
<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
require_once '../model/Word.php';
include_once '../model/Language.php';
include_once '../model/GramaticalClass.php';
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?= Config::TITLE ?></title>
    </head>

    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main>
            <div class="row">
                <?php
                if ($current_user instanceof User) {
                    ?>
                    <section class="col s0 l3">
                        <?php include '../view/partial/usermenu.php'; ?>
                    </section>
                    <?php
                }
                ?>
                <section class="col s12 <?= (!empty($current_user) ? 'l9' : 'l8 offset-l2') ?>">
                    <div class="row">
                        <div class="card">
                            <?php Helper::messageBoxRow(); ?>
                            <form action="../controller/WordController.php" method="POST" id="form-research">
                                <div class="row valign-wrapper">
                                    <div class="col s12 l8 offset-l2">
                                        <div class="card center-align">
                                            <div class="card-content green lighten-2">
                                                <div class="card-title center-align">Pesquise sua palavra aqui.</div>
                                            </div>
                                            <div class="card-content">
                                                <p class="grey-text text-darken-2 center-align">
                                                    Se não souber o idioma deixe em branco para a pesquisa ser mais abrangente.
                                                </p>
                                                <div class="input-field col s12 l4 valign">
                                                    <input type="text" class="validate" placeholder="Pesquisar" name="word_research" />
                                                </div>
                                                <div class="input-field col s12 l4 valign">
                                                    <?php
                                                    $language = new Language();
                                                    $list = $language->read();
                                                    Helper::createSelect(array('list' => $list,
                                                        'id' => 'language',
                                                        'name' => 'id_language',
                                                        'class' => 'id_language browser-default'), 'id_language', 'name', NULL);
                                                    ?>
                                                </div>
                                                <div class="input-field col s12 l4 valign">
                                                    <input type="hidden" class="btn green darken-2" name="option" value="research_word" />
                                                    <input type="submit" class=" col s12 btn green darken-2" value="PESQUISAR" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="card-content center-align">
                                <div class="card-title green-text text-darken-4"><b>Resultados de Sua Pesquisa</b></div>
                                <?php
                                if (isset($_GET['language_word'])) {
                                    Helper::messageBoxRow();
                                    require_once '../model/Word.php';
                                    require_once '../model/Translation.php';
                                    require_once '../model/Language.php';
                                    require_once '../model/LanguageWord.php';
                                    require_once '../model/GramaticalClass.php';
                                    require_once '../model/WordGramaticalClass.php';
                                    $id_language = (isset($_GET['id_language']) ? $_GET['id_language'] : NULL);
                                    $language_word = new LanguageWord($_GET['language_word'], $id_language);
                                    $word = new Word($language_word->id_word);
                                    $language = new Language($language_word->id_language);
                                    $word_class = new WordGramaticalClass();
                                    $data_word_class = $word_class->read(Array("id_word" => $word->id_word));
                                    $classes_word = '';
                                    foreach ($data_word_class AS $row => $fields) {
                                        $class = new GramaticalClass($fields['id_gramatical_class']);
                                        $classes_word = $classes_word . " " . $class->name;
                                    }
                                    $translation = new Translation();
                                    $lst = $translation->read(Array("id_language_word1" => $_GET['language_word']));
//                                    $lst = $word->listWordsResearch($_GET['language_word'], (isset($_GET['language']) ? $_GET['language'] : NULL));
                                    if ($word->id_word > 0) {
                                        ?>
                                        <div class="row">
                                            <div class="col s12">
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th data-field="language">Idioma</th>
                                                            <th data-field="word">Palavra</th>
                                                            <th data-field="plural">Plural</th>
                                                            <th data-field="class">Cl. Gram.</th>
                                                            <th data-field="sentence">Frase Ex.</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><?= $language->name ?></td>
                                                            <td><?= $word->word ?></td>
                                                            <td><?= $word->plural ?></td>
                                                            <td><?= $classes_word ?></td>
                                                            <td><?= $data_word_class[0]['example_sentence'] ?></td>
                                                        </tr>
                                                        <?php
                                                        if (count($lst) > 0) {
                                                            foreach ($lst AS $row => $data) {
                                                                $language_word = new LanguageWord($data['id_language_word2']);
                                                                $word = new Word($language_word->id_word);
                                                                $language = new Language($language_word->id_language);
                                                                $word_class = new WordGramaticalClass();
                                                                $data_word_class = $word_class->read(Array("id_word" => $word->id_word));
                                                                $classes_word = '';
                                                                foreach ($data_word_class AS $row => $fields) {
                                                                    $class = new GramaticalClass($fields['id_gramatical_class']);
                                                                    $classes_word = $classes_word . " " . $class->name;
                                                                }
                                                                ?>
                                                                <tr>
                                                                    <td><?= $language->name ?></td>
                                                                    <td><?= $word->word ?></td>
                                                                    <td><?= $word->plural ?></td>
                                                                    <td><?= $classes_word ?></td>
                                                                    <td><?= $data_word_class[0]['example_sentence'] ?></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        } else {
                                                            $lst_empty = true;
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <?php
                                        if (isset($lst_empty)) {
                                            ?>
                                            <div class="col s12 card-title green-text text-lighten-1">Palavra sem traduções</div>

                                            <div class="card-action">
                                                <?php
                                                if ($current_user instanceof User) {
                                                    ?>
                                                    <a href="#modal-register-word" class="modal-trigger btn waves-effect waves-light green darken-2 white-text">Cadastrar Palavra</a>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <a href="#modal-login" class="modal-trigger btn waves-effect waves-light green darken-2 white-text">Cadastrar Palavra</a>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="col s12 card-title green-text text-lighten-1">Nada encontrado...</div>
                                        <div class="card-action">
                                            <?php
                                            if ($current_user instanceof User) {
                                                ?>
                                                <a href="#modal-register-word" class="modal-trigger btn waves-effect waves-light green darken-2 white-text">Cadastrar Palavra</a>
                                                <?php
                                            } else {
                                                ?>
                                                <a href="#modal-login" class="modal-trigger btn waves-effect waves-light green darken-2 white-text">Cadastrar Palavra</a>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="col s12 card-title green-text text-lighten-1">Digite uma palavra e escolha um idioma para pesquisar.</div>
                                    <div class="card-action">
                                        <?php
                                        if ($current_user instanceof User) {
                                            if (!empty($lst) || !isset($_GET['language_word'])) {
                                                ?>
                                                <a href="#modal-register-word" class="modal-trigger btn waves-effect waves-light green darken-2 white-text">Cadastrar Palavra</a>
                                                <?php
                                            } else {
                                                ?>
                                                <a href="#modal-register-word" class="modal-trigger btn waves-effect waves-light green darken-2 white-text">Cadastrar Traduções</a>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <a href="#modal-login" class="modal-trigger btn waves-effect waves-light green darken-2 white-text">Cadastrar Palavra</a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }
                                ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php
            if ($current_user instanceof User) {
                ?>
                <div class="row">
                    <div id="modal-register-word" class="modal modal-fixed-footer col s12 m10 offset-m1 l10 offset-l1">
                        <div class="modal-content">
                            <?php include './partial/form_new_word.php'; ?>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="waves-effect waves-green btn-flat green darken-1 white-text" id="register-word">REGISTRAR</button>
                            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCELAR</a>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                ?>
                <?php include './partial/login-modal.php'; ?>
                <?php
            }
            ?>
            <p class="hideMe textTranslate"></p>
        </main>
        <?php include './partial/scripts.php'; ?>
        <script type="text/javascript" src="./js/jquery-translate-1.3.9.min.js"></script>
        <script>
            function translate(language) {
                switch (language) {
                    case '1':
                        $(".textTranslate").html($("#first_word").val());
                        translate = $('.textTranslate').translate('pt');
                        $("#second_word").val(translate);
                        break;
                }
            }
            $(document).ready(function () {
                $("#first_word").change(function () {
                    translate($('#id_first_language').val());
                });
                $("#category").change(function () {
                    $("#category1").val($("#category").val());
                    $("#category2").val($("#category").val());
                    $("#category3").val($("#category").val());
                });
                $('#id_first_language').change(function () {
                    id_language = $('.id_first_language').val();
                    if (id_language === '1') {
//                        $('.translate1').html("Inglês");
                        $('.translate1').html("Português");
                        $('#language1').val("2");
                        $('.translate2').html("Alemão");
                        $('#language2').val("3");
                        $('.translate3').html("Espanhol");
                        $('#language3').val("4");
                    } else if (id_language === '2') {
                        $('.translate1').html("Inglês");
                        $('#language1').val("1");
//                        $('.translate1').html("Português");
                        $('.translate2').html("Alemão");
                        $('#language2').val("3");
                        $('.translate3').html("Espanhol");
                        $('#language3').val("4");
                    } else if (id_language === '3') {
                        $('.translate1').html("Inglês");
                        $('#language1').val("1");
                        $('.translate2').html("Português");
                        $('#language2').val("2");
//                        $('.translate2').html("Alemão");
                        $('.translate3').html("Espanhol");
                        $('#language3').val("4");
                    } else if (id_language === '4') {
                        $('.translate1').html("Inglês");
                        $('#language1').val("1");
                        $('.translate2').html("Português");
                        $('#language2').val("2");
                        $('.translate3').html("Alemão");
                        $('#language3').val("3");
//                        $('.translate3').html("Espanhol");
                    }
                });
                $('.collapsible').collapsible({
                    accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
                });
                $('.modal-trigger').leanModal();
<?php
if ($current_user instanceof User) {
    if (isset($_GET['register'])) {
        ?>
                        $('#modal-register-word').openModal();
                        $(".modal-close").click(function () {
                            $('#modal-register-word').closeModal();
                        });
                        $("#register-word").click(function () {
                            $("#form-new-word").submit();
                        });
        <?php
    } else {
        ?>
                        $('#modal-register-word').openModal();
                        $('#modal-register-word').closeModal();
                        $("#register-word").click(function () {
                            $("#form-new-word").submit();
                        });
        <?php
    }
} else {
    ?>
                    $('#modal-login').openModal();
                    $('#modal-login').closeModal();
    <?php
}
?>
            });
        </script>
    </body>
</html>
