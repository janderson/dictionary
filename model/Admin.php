<?php

require_once '../model/Crud.php';

class Admin extends Crud {

    const URL_EDIT = '../admin/edt_admin.php';
    const URL_NEW = '../admin/new_admin.php';
    const URL_LIST = '../admin/lst_admin.php';
    
    protected $id_admin;
    protected $name;
    protected $email;
    protected $password;
    protected $_dtt_register;
    protected $user_control;
    protected $translation_control;
    protected $gramatical_control;
    protected $words_control;
    protected $admin_control;
    protected $_table = "ADMIN";
    protected $_list = 'lst_admin';
    protected $_key_field = 'id_admin';

    function __set($atribute, $value) {
        $this->$atribute = $value;
    }

    function __get($atribute) {
        return $this->$atribute;
    }

    function __construct($id = NULL) {
        parent::__construct();
        if ($id > 0) {
            $this->find($id);
        }
    }

    public function find($id) {
        if (parent::find(Array($this->_key_field => $id))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getSerialized($id = NULL) {
        $this->retriveAll(($id != NULL ? Array($this->_key_field => $id) : ''));
        $serialized = serialize($this);
        return $serialized;
    }

    public function __sleep() {
        return array('id_admin', 'name', 'email', 'cpf', 'phone', 'cellfone');
    }

    public function __wakeup() {
        return $this;
    }

    static public function getDescriptionKey($key) {
        try {
            switch ($key) {
                case 'id_admin':return 'ID';
                case '_id':return 'ID';
                case 'name':return 'Nome';
                case 'cpf':return 'CPF';
                case 'phone':return 'Telefone';
                case 'cellphone':return 'Celular';
                case 'adress':return 'Endereço';
                case 'email':return 'Email';
                case 'active':return 'Ativo';
                default : return $key;
            }
        } catch (Exception $exc) {
            return NULL;
        }
    }

    public function validateAdmin($email, $password) {
        /*
         * Método para validação de usuário apartir de E-mail e senha
         * 
         * @param String $cpf cpf para realizar busca
         * @param String $password senha para realizar busca
         * @return BOOL TRUE caso encontrado usuário registrado com cpf e senha passados, FALSE caso contrário
         */

        $data = $this->read(array("email" => $email, 'password' => $password));

        if (count($data) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function validateCredentials(Array $credentials) {
        $return = FALSE;
        if (count($credentials) > 0) {
            foreach ($credentials as $key) {
                if ($this->$key == 1) {
                    $return = TRUE;
                } else {
                    $return = FALSE;
                }
            }
        } else {
            $return = TRUE;
        }
        return $return;
    }

    public function retriveAdmin($email) {
        /*
         * Método específico para obter o array da usuário apartir do CPF passado por parâmetro
         * @param String $cpf CPF do usuário a ser buscado
         * $return Array $data Array de dados do usuário encontrada
         */
        $data = $this->retrive(array("email" => $email));

        return $data;
    }

    public function getList() {
        try {
            $stmt = $this->_db->prepare("SELECT `id_admin` AS `_id`, `id_admin` AS `ID`, `name`, `email` FROM"
                    . " `{$this->_table}` ORDER BY `name` ASC LIMIT 50");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }
    
    public function getUserRegistedToday() {
        try {
            $stmt = $this->_db->prepare("SELECT * FROM `USER` WHERE DATE_FORMAT(dtt_register, '%Y-%m-%d') = CURDATE()");
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return count($stmt->fetchAll());
            } else {
                $error = $stmt->errorInfo();
                Logger::logFor(__CLASS__ . '-list', "{$error[2]}: {$error[1]}");
                return NULL;
            }
        } catch (Exception $exc) {
            Logger::logFor(__CLASS__ . '-list', $exc->getTraceAsString());
            return NULL;
        }
    }

    public function updatePassword($password, $idadmin) {

        $stmt = $this->_db->prepare("UPDATE `$this->_table` SET password='$password' WHERE id_admin = $idadmin ;");

        $stmt->execute();

        $count = $stmt->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            $erros = $stmt->errorInfo();
            Logger::log_array(__CLASS__, $erros);
            throw new Exception('Sua senha não foi modificada.', $erros[1]);
        }
    }

    public function setDatabaseSession() {
        /*
         * Este método será utilizado para criar um log do módulo de financeiro no banco de dados através de Triggers
         */
        $stmt = $this->_db->query("SET @session_id_admin = $this->id_admin");
        if ($stmt !== FALSE) {
            return TRUE;
        } else {
            $erros = $stmt->errorInfo();
            Logger::log_array(__CLASS__, $erros);
            throw new Exception('Não foi possível identificar o administrador.', $erros[1]);
        }
    }

}

?>
