<?php

require_once("./Controller.php");
require_once("./ExceptionController.php");
require_once("../model/Contact.php");

class ContactController extends Controller {

    const ERROR_CODE_PREFIX = '04';
    const CONTACT_EMAIL = 'contato@inventum.org.br';

    private $contact;

    public function __construct() {
        parent::__construct();
        try {
            $this->contact = new Contact();
            switch ($_SERVER['REQUEST_METHOD']) {
                case "POST":
                    if (empty($_POST['option'])) {
                        throw new ExceptionController('Opção não foi setada!', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_OPTION, NULL);
                    }
                    switch ($_POST['option']) {
                        case 'send':
                            if ($this->regContactUser()) {
                                header("Location: ../view/contact.php");
                            }
                            break;
                        case 'send_answer':
                            $this->regContactAnswer();
                            break;
                        default:
                            break;
                    }
                    break;
                case "GET":
                    if (empty($_GET['option'])) {
                        throw new ExceptionController('Opção não foi setada!', self::ERROR_CODE_PREFIX . ExceptionController::INVALID_OPTION, NULL);
                    }
                    break;
            }
        } catch (Exception $e) {
            if ($e instanceof ExceptionController || $e instanceof CrudException) {
                $_SESSION['error_code'] = $e->getCode();
                $_SESSION['error_message'] = $e->getMessage();
                if (empty($_SERVER['HTTP_REFERER'])) {
                    header("Location: " . ExceptionController::PATH_ERROR_PAGE);
                } else {
                    header("Location: {$_SERVER['HTTP_REFERER']}");
                }
            } else {
                ExceptionController::handleException($e);
            }
        }
    }

    private function regContactUser() {
        if (!empty($_POST['email'])) {
            $this->contact->email = $_POST['email'];
        } else {
            throw new ExceptionController('Insira seu email.', self::ERROR_CODE_PREFIX . ExceptionController::EMPTY_INPUT);
        }
        if (!empty($_POST['name'])) {
            $this->contact->name = $_POST['name'];
        } else {
            throw new ExceptionController('Insira um nome.', self::ERROR_CODE_PREFIX . ExceptionController::EMPTY_INPUT);
        }
        if (!empty($_POST['subject'])) {
            $this->contact->subject = $_POST['subject'];
        } else {
            throw new ExceptionController('Insira um assunto.', self::ERROR_CODE_PREFIX . ExceptionController::EMPTY_INPUT);
        }
        if (!empty($_POST['content'])) {
            $this->contact->content = $_POST['content'];
        } else {
            throw new ExceptionController('Insira uma mensagem.', self::ERROR_CODE_PREFIX . ExceptionController::EMPTY_INPUT);
        }
        $this->contact->status = 0;
        $this->contact->contact_type = 1;
        $this->contact->id_user = $_POST['id_user'];

        $res = $this->contact->create(array('id_contact' => $this->contact->id_contact));
        if ($res > 0) {
            $_SESSION['sucess'] = 'Contato enviado com sucesso';
            return TRUE;
        } else {
            throw new ExceptionController("Falha ao realizar contato.", self::ERROR_CODE_PREFIX . ExceptionController::FAIL_REGISTER);
        }
    }

    private function regContactAnswer() {
        if (!empty($_POST['email'])) {
            $this->contact->email = $_POST['email'];
        } else {
            throw new ExceptionController('Insira seu email.', self::ERROR_CODE_PREFIX . ExceptionController::EMPTY_INPUT);
        }
        if (!empty($_POST['name'])) {
            $this->contact->name = $_POST['name'];
        } else {
            throw new ExceptionController('Insira um nome.', self::ERROR_CODE_PREFIX . ExceptionController::EMPTY_INPUT);
        }
        if (!empty($_POST['subject'])) {
            $this->contact->subject = $_POST['subject'];
        } else {
            throw new ExceptionController('Insira um assunto.', self::ERROR_CODE_PREFIX . ExceptionController::EMPTY_INPUT);
        }
        if (!empty($_POST['content'])) {
            $this->contact->content = $_POST['content'];
        } else {
            throw new ExceptionController('Insira uma mensagem.', self::ERROR_CODE_PREFIX . ExceptionController::EMPTY_INPUT);
        }
        $this->contact->status = 1;
        $this->contact->contact_type = 2;

        $this->sendEmail($this->contact->email, $this->contact->subject, $this->contact->content);
        $res = $this->contact->create(array('id_contact' => $this->contact->id_contact));

        if ($res > 0) {
            $_SESSION['sucess'] = 'Contato enviado com sucesso';
            header("Location: ../admin/inbox.php");
        } else {
            throw new ExceptionController("Falha ao realizar contato.", self::ERROR_CODE_PREFIX . ExceptionController::FAIL_REGISTER);
        }
    }

    private function sendEmail($to, $subject, $message) {
        $email = new Email();
        $email->sendRequestEmail($to, $subject, $message);
        return TRUE;
    }

    private function Exception($message, $cod) {
        Logger::log_array(__CLASS__, Array("Mensagem: ", $message, "Código: ", $cod));
        throw new ExceptionController($message, $cod);
    }

}

new ContactController();
