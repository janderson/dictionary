<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
Helper::requireUserSession($current_user);
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?= Config::TITLE ?></title>
    </head>
    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main>
            <div class="section">
                <div class="container">
                    <div class="row">
                        <?php
                        include_once '../model/State.php';
                        $state = new State();
                        $list_state = $state->read();

                        include_once '../model/City.php';
                        $city = new City();
                        $city->find($current_user->id_city);
                        $list_city = $city->getStateCities($state->id_state);
                        Helper::messageBoxRow();
                        ?>
                        <div class="col s12 l3 hide-on-med-and-down">
                            <?php include './partial/usermenu.php'; ?>
                        </div>
                        <div class="col s12 l9">
                            <div class="card-panel">
                                <form id="form-register" class="form-horizontal" method="POST" action="../controller/UserController.php">
                                    <h4 class="left-align  grey-text text-darken-2">Editar Seus perfil</h4>
                                    <?php Helper::createInput(array('type' => 'hidden', 'value' => $current_user->id_user, 'name' => 'id_user')); ?>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input placeholder="Digite seu nome (obrigatório)" id="name" type="text" name="name" value="<?php echo $current_user->name ?>" class="validate">
                                            <label for="name">Nome</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 l8">
                                            <input placeholder="Informe um e-mail válido" id="email" type="email" name="email" value="<?php echo $current_user->email ?>" class="validate" readonly="readonly">
                                            <label for="email">Email</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 l6">
                                            <input placeholder="Informe uma nova senha" id="password" type="password" name="new_password" class="validate">
                                            <label for="password">Senha</label>
                                        </div>
                                        <div class="input-field col s12 l6">
                                            <input placeholder="Repita a nova senha" id="re-password" type="password" name="re_password" class="validate">
                                            <label for="re-password">Repita sua Senha</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12 l6">
                                            <label class="left-align col s12">Estado</label>
                                            <div class="input-field col s12">
                                                <select id="id_state" class="browser-default">
                                                    <?php
                                                    echo '<option value="" disabled selected>Selecione...</option>';
                                                    foreach ($list_state as $row => $ob_state) {
                                                        if ($city->id_state == $ob_state['id_state']) {
                                                            echo "<option value={$ob_state['id_state']} selected='selected'>{$ob_state['name']}</option>";
                                                        } else {
                                                            echo "<option value={$ob_state['id_state']}>{$ob_state['name']}</option>";
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div id="load_cities" class="col s12 l6">
                                            <label class="left-align col s12">Cidade</label>
                                            <div class="input-field col s12" >
                                                <select  class="browser-default">
                                                    <?php
                                                    echo '<option value="" disabled selected>Selecione...</option>';
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button type="submit" name="option" value="edit" class="waves-effect waves-light btn">Enviar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php include '../view/partial/scripts.php'; ?>
        <script src="./js/activities.js"></script>
        <script>
            jQuery('#dt_born').datetimepicker({
                timepicker: false,
                format: 'd/m/Y'
            });

            $("#cpf").mask("999.999.999-99");
            $("#dt_born").mask("99/99/9999");
            $("#phone").mask("(99)9999-9999");
            $("#cellfone").mask("(99)9999-9999?9");
            $("#rg").mask("9999999999999");
            $("#postal_code").mask("99999-999");
        </script>
        <script>
            $(document).ready(function () {
                $('#id_state').change(function () {
                    var state = $('#id_state').val();
                    if (state) {
                        var url = '../controller/HelperController.php?idstate=' + state;
                        $.get(url, function (dataReturn) {
                            $('#load_cities').html(dataReturn);
                        });
                    }
                });
            });
        </script>
    </body>
</html>
