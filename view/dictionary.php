<?php
include '../view/partial/helper.php';
$current_user = Helper::bootSession();
require_once '../model/Word.php';
?>
<html>
    <head>
        <?php include '../view/partial/head.php'; ?>
        <title><?php echo Config::TITLE ?></title>
    </head>
    <body>
        <header>
            <?php include '../view/partial/header.php'; ?>
        </header>
        <main class="section">
            <?php
            Helper::messageBoxRow();
            ?>
            <div class="row">
                <?php
                if (empty($current_user)) {
                    include './partial/login.php';
                } else {
                    ?>
                    <div class="col s12 l3 hide-on-med-and-down">
                        <?php include './partial/usermenu.php'; ?>
                    </div>
                    <?php
                }
                ?>
                <div class="col s12 l9">
                    <div class="row">
                        <div class="card">
                            <div class="card-content">
                                <?php
                                $array = Helper::getArrayAlphabet();
                                Helper::createTabs($array);
                                ?>
                            </div>
                            <div class="card-content">
                                <?php
                                $word = new Word();
                                $data = $word->getListFilterByLetter((!empty($_GET['filter']) ? $_GET['filter'] : NULL));

//                                $options = Array(
//                                    Array("url" => "./dtl_word.php", "icon" => "chrome_reader_mode", "position" => "left", "daley" => "50", "tooltip" => "Mais Informações")
//                                );
                                Helper::createTableNovo(array("model" => "Word", "list" => $data, "html_class" => "striped", "line_number" => FALSE));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php include '../view/partial/scripts.php'; ?>
    </body>
</html>