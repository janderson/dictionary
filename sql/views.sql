/*
ATUALIZAÇAO 09/05

Views desatualizadas, não executalas no BD
*/

/*LISTA DE PALAVRAS*/

CREATE VIEW get_words AS SELECT 
w.id_word as _id, 
w.`word` as `word`, 
w.`plural` as `plural`,
c.`name` as `category`,
l.`name` as `name_language`,
g.`name` as `class`,
wg.`example_sentence` as `example_sentence`
FROM `WORD` as w 
INNER JOIN `CATEGORY` as `c` ON c.id_category = w.id_category
LEFT JOIN `LANGUAGE_WORD` as `lw` ON lw.id_word = w.id_word
LEFT JOIN `WORD_GRAMATICAL_CLASS` as `wg` ON wg.id_word = w.id_word
INNER JOIN `GRAMATICAL_CLASS` as `g` ON g.id_gramatical_class = wg.`id_gramatical_class`
LEFT JOIN `LANGUAGE` as `l` ON l.id_language = lw.id_language;
-- 
CREATE VIEW get_word_untranslated AS SELECT 
w.id_word as _id, 
w.`word` as `word`, 
c.`name` as `category`, 
l.`name` as `language`, 
w.`plural` as `plural`
FROM `LANGUAGE_WORD` as lw 
LEFT JOIN `WORD` as `w` ON w.id_word = lw.id_word
INNER JOIN `CATEGORY` as `c` ON c.id_category = w.id_category
LEFT JOIN `LANGUAGE` as `l` ON l.id_language = lw.id_language
WHERE lw.`id_word` not in (SELECT `id_language_word1` FROM TRANSLATION);

/*LISTA DE IDIOMAS*/

CREATE VIEW get_languages AS SELECT 
l.id_language as _id, 
l.`name` as `name`, 
l.`home_country` as `home_country`, 
l.`description` as `description`
FROM `LANGUAGE` as l;

/*LISTA DE CLASSES GRAMATICAIS*/

CREATE VIEW get_classes AS SELECT 
gc.id_gramatical_class as _id, 
gc.`name` as `name`, 
gc.`description` as `description`
FROM `GRAMATICAL_CLASS` as gc;

-- CREATE VIEW get_words_by_filter AS SELECT 
-- w.id_word as _id, 
-- w.`word` as `word`, 
-- w.`plural` as `plural`,
-- c.`name` as `category`,
-- g.`name` as `name_class`, 
-- l.`name` as `name_language`,
-- t.`id_language` as `_id_language`,
-- wg.`example_sentence` as `example_sentence`, 
-- wg.`id_gramatical_class` as `_id_class` 
-- FROM `WORD` as w 
-- INNER JOIN `CATEGORY` as `c` ON c.id_category = w.id_category
-- INNER JOIN `TRANSLATION` as `t` ON t.id_word = w.id_word
-- INNER JOIN `LANGUAGE` as `l` ON l.id_language = t.id_language
-- INNER JOIN `WORD_GRAMATICAL_CLASS` as `wg` ON wg.id_word = w.id_word
-- INNER JOIN `GRAMATICAL_CLASS` as `g` ON g.id_gramatical_class = wg.`id_gramatical_class`;
