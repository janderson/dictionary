jQuery.extend(jQuery.validator.messages, {
    required: "Campo Obrigatório.",
    remote: "Verifique este campo.",
    email: "Digite um e-mail válido.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Um número deve ser informado.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "As informações não são iguais.",
    accept: "Please enter a value with a valid extension.",
    message: "Mensagem vazia",
    maxlength: jQuery.validator.format("Informe no máximo {0} caracteres."),
    minlength: jQuery.validator.format("Informe no mínimo {0} caracteres."),
    rangelength: jQuery.validator.format("Informe entre {0} e {1} caracteres."),
    range: jQuery.validator.format("O valor deve estar entre {0} e {1}."),
    max: jQuery.validator.format("O valor precisa ser menor ou igual a {0}."),
    min: jQuery.validator.format("O valor precisa ser maior ou igual a {0}.")
});

var Script = function () {
    /*
     * Brazillian CPF number (Cadastrado de Pessoas Físicas) is the equivalent of a Brazilian tax registration number.
     * CPF numbers have 11 digits in total: 9 numbers followed by 2 check numbers that are being used for validation.
     */
    $.validator.addMethod("cpfBR", function (value) {
        // Removing special characters from value
        value = value.replace(/([~!@#$%^&*()_+=`{}\[\]\-|\\:;'<>,.\/? ])+/g, "");

        // Checking value to have 11 digits only
        if (value.length !== 11) {
            return false;
        }

        var sum = 0,
                firstCN, secondCN, checkResult, i;

        firstCN = parseInt(value.substring(9, 10), 10);
        secondCN = parseInt(value.substring(10, 11), 10);

        checkResult = function (sum, cn) {
            var result = (sum * 10) % 11;
            if ((result === 10) || (result === 11)) {
                result = 0;
            }
            return (result === cn);
        };

        // Checking for dump data
        if (value === "" ||
                value === "00000000000" ||
                value === "11111111111" ||
                value === "22222222222" ||
                value === "33333333333" ||
                value === "44444444444" ||
                value === "55555555555" ||
                value === "66666666666" ||
                value === "77777777777" ||
                value === "88888888888" ||
                value === "99999999999"
                ) {
            return false;
        }

        // Step 1 - using first Check Number:
        for (i = 1; i <= 9; i++) {
            sum = sum + parseInt(value.substring(i - 1, i), 10) * (11 - i);
        }

        // If first Check Number (CN) is valid, move to Step 2 - using second Check Number:
        if (checkResult(sum, firstCN)) {
            sum = 0;
            for (i = 1; i <= 10; i++) {
                sum = sum + parseInt(value.substring(i - 1, i), 10) * (12 - i);
            }
            return checkResult(sum, secondCN);
        }
        return false;

    }, "Por favor especificar um número de CPF válido");

    $.validator.addMethod("selects", function (value, element, arg) {
        return arg != value;
    }, "Valor deve não igual arg.");

    $().ready(function () {
        $("#form-register").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 5
                },
                mothername: {
                    required: true,
                    minlength: 5
                },
                dt_born: {
                    required: true,
                    minlength: 5
                },
                password: {
                    required: true,
                    minlength: 4
                },
                email: {
                    required: false,
                    email: true
                },
                re_password: {
                    minlength: 4,
                    equalTo: "#password"
                },
                id_state: {
                    required: true,
                    selects: true
                },
                combobox_city: {
                    required: true,
                    selects: true
                },
                cpf: {
                    cpfBR: true,
                    minlength: 11
                }
            },
            messages: {
                name: {
                    required: "Por favor, digite seu nome",
                    minlength: "Por favor, digite seu nome completo"
                },
                password: {
                    required: "Digite uma senha.",
                    minlength: "Sua senha deve conter no mínimo 5 caracteres"
                },
                re_password: {
                    minlength: "Sua senha deve conter no mínimo 5 caracteres",
                    equalTo: "A senha deve ser idêntica a informada acima"
                },
                email: {
                    required: "Por favor, digite seu e-mail",
                    email: "Por favor, digite um email válido."
                },
                id_state: {
                    required: "Por favor, selecione seu estado"
                },
                combobox_city: {
                    required: "Por favor, selecione sua cidade"
                },
                cpf: {
                    required: 'Digite um CPF!',
                    cpfBR: 'CPF precisa ser válido!',
                    minlength: 'CPF possúi 11 caracteres.'
                },
                agree: "Por favor, você deve aceitar nossos termos de compromisso."
            }
        });

        $("#form-contact").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 5
                },
                email: {
                    required: true,
                    email: true
                },
                subject: {
                    minlength: 4,
                    required: true,
                },
                content: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "Por favor, digite seu nome",
                    minlength: "Por favor, digite seu nome completo"
                },
                email: {
                    required: "Por favor, digite seu e-mail",
                    email: "Por favor, digite um email válido."
                },
                subject: {
                    required: "Por favor, digite o assunto"
                },
                content: {
                    required: 'Digite uma mensagem!',
                    minlength: 'mínimo de 4 caracteres.'
                },
                agree: "Por favor, você deve aceitar nossos termos de compromisso."
            }
        });

        $("#form-login").validate({
            rules: {
                email: {
                    required: true,
                    email: false
                },
                login: {
                    required: true
                },
                password: {
                    required: true,
                    minlength: 4
                }
            },
            messages: {
                email: {
                    required: "Por favor, digite seu e-mail ou cpf",
                    email: "Por favor, digite um email válido."
                },
                login: {
                    required: "Por favor, digite seu e-mail ou cpf"
                },
                password: {
                    required: "Por favor, digite uma senha",
                    minlength: "Sua senha deve conter no mínimo 5 caracteres"
                }
            }
        });

        // propose username by combining first- and lastname
        $("#username").focus(function () {
            var firstname = $("#firstname").val();
            var lastname = $("#lastname").val();
            if (firstname && lastname && !this.value) {
                this.value = firstname + "." + lastname;
            }
        });

    });
}();